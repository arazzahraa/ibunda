package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.Answer
import kotlinx.android.synthetic.main.item_answer.view.*


class AnswerAdapter(val context: Context, val datas: MutableList<Answer>): RecyclerView.Adapter<AnswerAdapter.AnswerViewHolder>()
{
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnswerViewHolder {
       return AnswerViewHolder(LayoutInflater.from(context).inflate(R.layout.item_answer,parent,false))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: AnswerViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class AnswerViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){
        fun bind(answer: Answer){
            with(itemView){
                rbAnswer.isChecked=answer.isSelected
                rbAnswer.text=answer.answer
                rbAnswer.setOnClickListener { onItemSelected(adapterPosition) }
            }
        }
    }

    fun onItemSelected(posisition:Int){
        datas.forEachIndexed { index, answer ->
            answer.isSelected = index==posisition
        }
        notifyDataSetChanged()
    }
}