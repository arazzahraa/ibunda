package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.Chat
import kotlinx.android.synthetic.main.item_chat_psikolog.view.*

class ChatAdapter(
    val context: Context,
    val datas: MutableList<Chat>,
    var currentUserId: String = "") :
    RecyclerView.Adapter<ChatAdapter.ChatViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatViewHolder {
        return when (viewType) {
            R.layout.item_chat_pengguna -> ChatViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_chat_pengguna,
                    parent,
                    false
                )
            )
            R.layout.item_chat_psikolog -> ChatViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_chat_psikolog,
                    parent,
                    false
                )
            )
            else -> ChatViewHolder(
                LayoutInflater.from(context).inflate(
                    R.layout.item_chat_psikolog,
                    parent,
                    false
                )
            )
        }

    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ChatViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    override fun getItemViewType(position: Int): Int {
        return if (datas[position].idSender == currentUserId) {
            R.layout.item_chat_pengguna
        } else {
            R.layout.item_chat_psikolog
        }
    }


    inner class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(chat: Chat) {
            with(itemView) {
                tvChat.text = chat.chat
            }
        }
    }

    fun add(item: Chat) {
        datas.add(item)
        notifyItemInserted(datas.size - 1)
    }

    fun addOrUpdate(items: List<Chat>) {
        val size = items.size
        for (i in 0 until size) {
            val item = items[i]
            val x = datas.indexOf(item)
            if (x >= 0) {
                datas.set(x, item)
            } else {
                add(item)
            }
        }
        notifyDataSetChanged()
    }

}

