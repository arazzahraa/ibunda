package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.Location
import com.ara.ibunda.model.Psikolog
import kotlinx.android.synthetic.main.item_location.view.*

class LocationAdapter(
    val context: Context,
    val datas: MutableList<Location>,
    val listener: (location: Location) -> Unit
) : RecyclerView.Adapter<LocationAdapter.LocationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        return LocationViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_location,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class LocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(location: Location) {
            with(itemView) {
                name.text = location.name
                address.text = location.address

                setOnClickListener {
                    listener.invoke(location)
                }
            }
        }
    }
}
