package com.ara.ibunda.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.ConfirmPayment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_confirm_admin.view.*
import javax.sql.DataSource

class ConfirmPaymentAdapter(
    val context: Context,
    val datas: MutableList<ConfirmPayment>,
    val listener: (confirmPayment: ConfirmPayment) -> Unit
) :
    RecyclerView.Adapter<ConfirmPaymentAdapter.ConfirmPaymentViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConfirmPaymentViewHolder {
        return ConfirmPaymentViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_confirm_admin,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ConfirmPaymentViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class ConfirmPaymentViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(confirmPayment: ConfirmPayment) {
            with(itemView) {
                with(itemView) {
                    var requestOption = RequestOptions()
                        .placeholder(R.drawable.ic_profile)
                        .error(R.drawable.ic_profile)
                    Glide.with(this).load(confirmPayment.patientAvatar).listener(object :
                        RequestListener<Drawable> {
                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: com.bumptech.glide.request.target.Target<Drawable>?,
                            dataSource: com.bumptech.glide.load.DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: com.bumptech.glide.request.target.Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            ivAvatarConfirmPembayaran.setImageResource(R.drawable.ic_profile)
                            return false
                        }
                    }).apply(requestOption).into(ivAvatarConfirmPembayaran)

                    namaPengguna.text = confirmPayment.name

                    setOnClickListener {
                        listener.invoke(confirmPayment)

                    }
                }

            }
        }
    }
}