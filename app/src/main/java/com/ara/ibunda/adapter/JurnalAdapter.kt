package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.Jurnal
import kotlinx.android.synthetic.main.item_rekam_kasus.view.*

class JurnalAdapter(
    val context: Context,
    val datas: MutableList<Jurnal>,
    val listener: (jurnal: Jurnal) -> Unit
) : RecyclerView.Adapter<JurnalAdapter.jurnalViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): jurnalViewHolder {
        return jurnalViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_rekam_kasus,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: jurnalViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class jurnalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(jurnal: Jurnal) {
            with(itemView) {
                tvDateRekamKasus.text = jurnal.fullDate

                setOnClickListener {
                    listener.invoke(jurnal)
                }
            }
        }


    }

}