package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.ListSchedulePsikolog
import com.ara.ibunda.model.SchedulePsikolog
import kotlinx.android.synthetic.main.item_chat_counseling.view.*
import kotlinx.android.synthetic.main.item_list_schedule_psikolog.view.*
import kotlinx.android.synthetic.main.item_schedule_psikolog.view.*

class ListSchedulePsikologAdapter(val context: Context, val datas: MutableList<ListSchedulePsikolog>, val listener:()->Unit): RecyclerView.Adapter<ListSchedulePsikologAdapter.ListSchedulePsikologViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ListSchedulePsikologViewHolder {
        return ListSchedulePsikologViewHolder(LayoutInflater.from(context).inflate(R.layout.item_list_schedule_psikolog,parent,false))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: ListSchedulePsikologViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class ListSchedulePsikologViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listSchedulePsikolog: ListSchedulePsikolog) {
            with(itemView) {
                monthListSchedulePskolog.text = listSchedulePsikolog.month

                var schedulePsikologAdapter= SchedulePsikologAdapter(context, listSchedulePsikolog.content,{})
                rvSchedulePsikolog.apply {
                    layoutManager= LinearLayoutManager(context)
                    adapter= schedulePsikologAdapter
                }
            }
        }
    }
}