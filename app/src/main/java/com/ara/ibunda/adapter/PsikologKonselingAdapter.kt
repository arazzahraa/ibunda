package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.Konseling
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_history_konseling.view.*

class PsikologKonselingAdapter(
    val context: Context,
    val datas: MutableList<Konseling>,
    val listener: (konseling:Konseling) -> Unit
) : RecyclerView.Adapter<PsikologKonselingAdapter.KonselingViewHolder>() {

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: KonselingViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KonselingViewHolder {
        return KonselingViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_history_konseling, parent, false)
        )
    }

    inner class KonselingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(konseling: Konseling) {
            with(itemView) {
                Glide.with(this).load(konseling.psikologAvatar).into(ivAvatarPsikologKonseling)
                tvPsikologKonseling.text = konseling.psikolog

                setOnClickListener {
                    listener.invoke(konseling)
                }
            }
        }
    }
}