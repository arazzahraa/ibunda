package com.ara.ibunda.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.RekamKasusActivity
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.enumtype.RekamKasusType
import com.ara.ibunda.model.Location
import com.ara.ibunda.model.Konseling
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.item_chat_counseling.view.*
import javax.sql.DataSource

class KonselingAdapter(
    val context: Context,
    val datas: MutableList<Konseling>,
    val listener: (konseling:Konseling) -> Unit,
    val rekamKasusListener:(konseling:Konseling) -> Unit
) : RecyclerView.Adapter<KonselingAdapter.KonselingViewHolder>() {

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: KonselingViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KonselingViewHolder {
        return KonselingViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_chat_counseling, parent, false)
        )
    }

    inner class KonselingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(konseling: Konseling) {
            with(itemView) {
                var requestOption = RequestOptions()
                    .placeholder(R.drawable.ic_profile)
                    .error(R.drawable.ic_profile)
                Glide.with(this).load(konseling.patientAvatar).listener(object:
                    RequestListener<Drawable> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: com.bumptech.glide.load.DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        circleImageView2.setImageResource(R.drawable.ic_profile)
                        return false
                    }

                }).apply(requestOption).into(circleImageView2)

                namePsikolog.text = konseling.patientName

                mbKonseling.setOnClickListener {
                    listener.invoke(konseling)
                }

                mbIsiRekamKasus.setOnClickListener {
//                    intent.putExtra("rekam kasus", RekamKasusType.ISI.type)
                    rekamKasusListener.invoke(konseling)
                }

                if(konseling.paymentStatus==PaymentStatusType.SELESAI.type){
                    mbIsiRekamKasus.visibility= View.GONE
                }
                else {
                    mbIsiRekamKasus.visibility= View.VISIBLE
                }
            }
        }
    }
}