package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.SchedulePsikolog
import com.ara.ibunda.utils.getDayFromDate
import kotlinx.android.synthetic.main.item_schedule_psikolog.view.*

class SchedulePsikologAdapter(
    val context: Context,
    val datas: MutableList<SchedulePsikolog>,
    val listener: () -> Unit
) : RecyclerView.Adapter<SchedulePsikologAdapter.schedulePsikologViewHolder>() {
    override fun onBindViewHolder(holder: schedulePsikologViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SchedulePsikologAdapter.schedulePsikologViewHolder {
        return schedulePsikologViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_schedule_psikolog,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    inner class schedulePsikologViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(schedulePsikolog: SchedulePsikolog) {
            with(itemView) {
                tvdDatetListSchedulePskolog.text =
                    schedulePsikolog.getScheduleDateToString()
                timetListSchedulePskolog.text =
                    "${schedulePsikolog.timeStart}-${schedulePsikolog.timeEnd}"

                contenttListSchedulePskolog.text =
                    if (schedulePsikolog.isAvailable) "Available untuk\nKonseling" else "Konseling dengan\n ${schedulePsikolog.namePatient}"

            }
        }
    }
}