package com.ara.ibunda.adapter

import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.PsikologProfileActivity
import com.ara.ibunda.R
import com.ara.ibunda.model.Psikolog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_psikolog.view.*

class PsikologAdapter(
    val context: Context,
    val datas: MutableList<Psikolog>,
    val listener: (psikolog: Psikolog) -> Unit
) : RecyclerView.Adapter<PsikologAdapter.PsikologViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PsikologViewHolder {
        return PsikologViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_psikolog,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: PsikologViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class PsikologViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(psikolog: Psikolog) {
            with(itemView) {
                var requestOption = RequestOptions()
                    .placeholder(R.drawable.ic_profile)
                    .error(R.drawable.ic_profile)
                Glide.with(context).load(psikolog.avatar).listener(object :
                    RequestListener<Drawable> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: com.bumptech.glide.load.DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        ivAvatarPsikolog.setImageResource(R.drawable.ic_profile)
                        return false
                    }

                }).apply(requestOption).into(ivAvatarPsikolog)
                namePsikolog.text = psikolog.name

                setOnClickListener {
                    listener.invoke(psikolog)
                }

                mbProfilPsikolog.setOnClickListener {
                    val intent = Intent(context, PsikologProfileActivity::class.java)
                    intent.putExtra("psikolog", psikolog)
                    context.startActivity(intent)

                }
            }
        }
    }
}