package com.ara.ibunda.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.RekamKasusActivity
import com.ara.ibunda.model.Location
import com.ara.ibunda.model.Patient
import kotlinx.android.synthetic.main.item_chat_counseling.view.*

class PatientAdapter(
    val context: Context,
    val datas: MutableList<Patient>,
    val listener: (idPatient: String) -> Unit,
    val rekamKasusListener:(patient:Patient) -> Unit
) : RecyclerView.Adapter<PatientAdapter.PatientViewHolder>() {

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: PatientViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PatientViewHolder {
        return PatientViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_chat_counseling, parent, false)
        )
    }

    inner class PatientViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(patient: Patient) {
            with(itemView) {
                namePsikolog.text = patient.name

                mbKonseling.setOnClickListener {
                    listener.invoke(patient.id)
                }

                mbIsiRekamKasus.setOnClickListener {
                    rekamKasusListener.invoke(patient)
                }
            }
        }
    }
}