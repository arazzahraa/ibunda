package com.ara.ibunda.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ara.ibunda.R
import com.ara.ibunda.model.RekamKasus
import kotlinx.android.synthetic.main.item_rekam_kasus.view.*

class RekamKasusAdapter(
    val context: Context,
    val datas: MutableList<RekamKasus>,
    val listener: (rekamKasus:RekamKasus) -> Unit
) : RecyclerView.Adapter<RekamKasusAdapter.RekamKasusViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RekamKasusViewHolder {
        return RekamKasusViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_rekam_kasus,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: RekamKasusViewHolder, position: Int) {
        holder.bind(datas[position])
    }

    inner class RekamKasusViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(rekamKasus: RekamKasus) {
            with(itemView){
                tvDateRekamKasus.text = rekamKasus.date

                setOnClickListener {
                    listener.invoke(rekamKasus)
                }
            }
        }


    }
}