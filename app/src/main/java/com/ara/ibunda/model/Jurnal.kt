package com.ara.ibunda.model

import android.os.Parcelable
import com.ara.ibunda.utils.getFullDate
import com.ara.ibunda.utils.toFirestoreTimestamp
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize

data class Jurnal
    (
    var idUser: String = "",
    var dateJurnal: com.google.firebase.Timestamp = Date().toFirestoreTimestamp(),
    var fullDate: String = "",
    var mood: Int = 0,
    var quetionOne: String = "",
    var quetionTwo: String = "",
    var quetionThree: String = "",
    var quetionFour: String = "",
    var quetionFive: String = ""
) : Parcelable {
    fun getReadableDate(): String {
        return getFullDate(dateJurnal.toDate())

    }
}

