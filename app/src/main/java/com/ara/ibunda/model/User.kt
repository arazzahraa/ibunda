package com.ara.ibunda.model

class User (
    var id: String="",
    var email: String="",
    var namaLengkap: String="",
    var password: String="",
    var gender: String="",
    var age: String="",
    var typePsikolog: String="",
    var domisili: String="",
    var pengalaman: String="",
    var typeAdmin: String="",
    var status: String="",
    val avatar:String = ""){

    fun toPsikolog(): Psikolog{
        return Psikolog(
            id=id,
            email = email,
            name = namaLengkap,
            gender = gender,
            age = age,
            typePsikolog = typePsikolog,
            password = password,
            status = status,
            avatar = avatar
        )
    }

    fun toConfirmPayment():ConfirmPayment{
        return ConfirmPayment(
            id= id,
            name = namaLengkap,
            patientAvatar = avatar
        )
    }

    fun toPatient():Patient{
        return Patient(
            id= id,
            name = namaLengkap
        )
    }
}