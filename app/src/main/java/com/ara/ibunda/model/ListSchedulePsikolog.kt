package com.ara.ibunda.model

data class ListSchedulePsikolog(
    var month: String = "",
    var content
    : MutableList<SchedulePsikolog> = mutableListOf()
) {
}