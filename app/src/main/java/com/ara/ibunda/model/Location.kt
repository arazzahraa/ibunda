package com.ara.ibunda.model

data class Location(var id: String="", var city: String="", var name: String="", var address: String="") {
}