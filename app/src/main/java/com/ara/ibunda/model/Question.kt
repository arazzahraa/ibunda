package com.ara.ibunda.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Question(var title:String, var answer:List<Answer>) : Parcelable {
}