package com.ara.ibunda.model

import com.ara.ibunda.enumtype.PaymentStatusType

data class Payment(
    var id: String = "",
    var idKonseling: String = "",
    var idUser: String = "",
    var paymentReceipt: String = "",
    var paymentStatus: Int = PaymentStatusType.BELUMBAYAR.type)