package com.ara.ibunda.model

import com.ara.ibunda.utils.toFirestoreTimestamp
import com.google.type.Date
import java.sql.Timestamp

data class Chat(
    var idChatRoom: String="",
    var idSender: String="",
    var chat: String="",
    var createdAt: com.google.firebase.Timestamp = java.util.Date().toFirestoreTimestamp(),
    var isOwner: Boolean= false)