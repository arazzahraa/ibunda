package com.ara.ibunda.model

import android.os.Parcelable
import com.ara.ibunda.enumtype.UserType
import kotlinx.android.parcel.Parcelize
import java.util.*


@Parcelize
data class Psikolog(
    var id: String="",
    var name: String="",
    var email: String="",
    var username: String="",
    var gender: String="",
    var age: String="",
    var typePsikolog: String="",
    var password: String="",
    var status: String=UserType.PSIKOLOG.type,
    val avatar:String=""
) : Parcelable {

}