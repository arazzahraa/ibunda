package com.ara.ibunda.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Patient(
    var name:String,
    var id: String) : Parcelable {
}