package com.ara.ibunda.model

import android.os.Parcelable
import com.ara.ibunda.utils.convertData
import kotlinx.android.parcel.Parcelize

@Parcelize

data class Konseling(
    var id: String = "",
    var idSchedule: String = "",
    var idUser: String= "",
    var patientName: String= "",
    var patientAvatar: String= "",
    var service: Int = 0,
    var date: String = "",
    var hour: String = "",
    var location: String = "",
    var idPsikolog: String = "",
    var psikologAvatar: String= "",
    var psikolog: String = "",
    var price: String ="",
    var paymentStatus: Int = 0,
    var typePsikolog:String = ""
) : Parcelable {

    fun getMonthKonseling():String{
        return convertData(date, "EEEE,dd MMMM yyyy", "MMMM")
    }
}


