package com.ara.ibunda.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Answer (var answer:String, var isSelected:Boolean=false) : Parcelable {
}