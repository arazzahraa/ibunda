package com.ara.ibunda.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize

class ConfirmPayment(
    var id: String="",
    var name:String="",
    var service: String="",
    var typeKonseling: String="",
    var price: String="",
    var patientAvatar: String = "",
    var confirm: Boolean=false) : Parcelable {

}