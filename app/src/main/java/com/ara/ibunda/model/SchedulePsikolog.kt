package com.ara.ibunda.model

import android.os.Parcelable
import com.ara.ibunda.utils.getCurrentFirestoreTimestamp
import com.ara.ibunda.utils.getDayFromDate
import com.ara.ibunda.utils.getFullDate
import com.ara.ibunda.utils.getMonthFromDate
import com.google.firebase.Timestamp
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class SchedulePsikolog(
    var id: String = "",
    var timeStamp: Timestamp = getCurrentFirestoreTimestamp() ,
    var timeStart: String = "",
    var timeEnd: String = "",
    var repeat: String = "",
    var isAvailable: Boolean = false,
    var idPsikolog: String = "",
    var typePsikolog: String = "",
    var namePatient: String = ""

) : Parcelable {
    fun getScheduleMonth():String{
        return getMonthFromDate(timeStamp.toDate())
    }

    fun getScheduleDateToString():String{
        return  getDayFromDate(timeStamp.toDate())
    }

    fun getScheduleDate():Date{
        return timeStamp.toDate()
    }

    fun getFullDate():String{
        return  getFullDate(timeStamp.toDate())
    }
}