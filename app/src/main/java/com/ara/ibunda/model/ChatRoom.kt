package com.ara.ibunda.model

data class ChatRoom(
    var idChatRoom: String ="",
    var idPasien: String="",
    var idPsikolog: String=""
)