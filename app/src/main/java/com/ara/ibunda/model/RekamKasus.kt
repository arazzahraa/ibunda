package com.ara.ibunda.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RekamKasus(
    var id: String = "",
    var date: String="",
    var idKonseling: String="",
    var idUser: String="",
    var idPsikolog: String="",
    var subyektif: String = "",
    var obyektif: String = "",
    var assesment: String = "",
    var planning: String = ""
) : Parcelable {

}