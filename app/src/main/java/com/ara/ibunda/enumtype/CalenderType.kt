package com.ara.ibunda.enumtype

enum class CalenderType (
    val type : Int
){
    JURNAL(0), CURHAT(1), ECOUNSELING (2), COUNSELINGCORNER (3)
}