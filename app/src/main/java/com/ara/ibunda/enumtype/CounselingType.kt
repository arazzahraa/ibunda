package com.ara.ibunda.enumtype

enum class CounselingType (
    val type : Int
){
    BASIC(0), ESSENTIAL (1), PROFESIONAL (2), PREMIUM (3)
}