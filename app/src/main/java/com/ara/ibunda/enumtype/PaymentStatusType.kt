package com.ara.ibunda.enumtype

enum class PaymentStatusType(
    val type: Int
){
    BELUMBAYAR (0), MENUNGGUKONFIRMASI (1), BAYAR(2), SELESAI(3), REJECT (4)
}