package com.ara.ibunda.enumtype

enum class RepeatType (
    val type : Int
){
    SETIAPHARI (0), DUAHARI (1), SEMINGGU (2), DUAMINGGU (3), NOREPEAT (4)
}