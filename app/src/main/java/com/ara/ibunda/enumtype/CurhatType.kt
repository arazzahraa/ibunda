package com.ara.ibunda.enumtype

enum class CurhatType (
    val type : Int
){
    CURHAT(0), ECOUNSELING(1), COUNSELINGCORNER(2)
}
