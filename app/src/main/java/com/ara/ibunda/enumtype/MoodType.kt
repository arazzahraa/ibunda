package com.ara.ibunda.enumtype

enum class MoodType (
    val type : Int
){
    CRY(5), SAD (4), AVERANGE (3), SMILE (2), HAPPY (1)
}