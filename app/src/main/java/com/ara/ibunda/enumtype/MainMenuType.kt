package com.ara.ibunda.enumtype

enum class MainMenuType (
    val type : Int
){
    INFORMATION(0), COUNSELINGCORNER (1)
}