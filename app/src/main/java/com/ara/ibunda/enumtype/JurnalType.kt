package com.ara.ibunda.enumtype

enum class JurnalType (
    val type : Int
){
    ISI(0), LIHATPASIEN (1), LIHATPSIKOLOG (2)
}