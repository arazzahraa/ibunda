package com.ara.ibunda.enumtype

enum class UserType(
    val type: String
) {
    USER ("User"), PSIKOLOG ("Psikolog"), ADMIN ("Admin")
}