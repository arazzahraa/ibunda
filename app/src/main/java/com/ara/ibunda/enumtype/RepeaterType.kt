package com.ara.ibunda.enumtype

enum class RepeaterType (val type:Int){
    NO_REPEAT(0),
    DAILY(1),
    TWO_DAY(2),
    WEEKLY(3),
    TWO_WEEKLY(4)
}