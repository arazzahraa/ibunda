package com.ara.ibunda.enumtype

enum class RekamKasusType (
    val type : Int
){
    ISI (0), LIHAT (1)
}