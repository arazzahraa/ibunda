package com.ara.ibunda.enumtype

enum class QuestionType (
    val type : Int
){
    ECOUNSELING (0), COUNSELINGCORNER (1)
}