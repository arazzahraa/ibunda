package com.ara.ibunda.utils

import com.anychart.APIlib
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.core.cartesian.series.Line
import com.anychart.data.Mapping
import com.anychart.data.Set
import com.anychart.enums.Anchor
import com.anychart.enums.HoverMode
import com.anychart.enums.LabelsOverlapMode
import com.anychart.enums.MarkerType
import com.anychart.enums.Orientation
import com.anychart.enums.Position
import com.anychart.enums.ScaleStackMode
import com.anychart.enums.TooltipDisplayMode
import com.anychart.enums.TooltipPositionMode
import com.anychart.graphics.vector.Stroke

fun AnyChartView.toLineChart(title: String, yAxis: String, datas: List<DataEntry>, datasSeries: List<String>) {
    APIlib.getInstance().setActiveAnyChartView(this)

    val cartesian = AnyChart.line()

    cartesian.credits().enabled(false)

    cartesian.animation(true)

    cartesian.padding(10.0, 20.0, 5.0, 20.0)

    cartesian.crosshair().enabled(true)
    cartesian.crosshair()
        .yLabel(true)
        .yStroke(null as Stroke?, null, null, null as String?, null as String?)

    cartesian.tooltip().positionMode(TooltipPositionMode.POINT)

    cartesian.title(title)

    cartesian.yAxis(0).title(yAxis)
    cartesian.xAxis(0).labels().padding(5.0, 5.0, 5.0, 5.0)

    val set = Set.instantiate()
    set.data(datas)

    val seriesMapping = arrayOfNulls<Mapping>(datas.size)

    for (i in datasSeries.indices) {
        when (i) {
            0 -> seriesMapping[0] = set.mapAs("{ x: 'x', value: 'value' }")
            1 -> seriesMapping[1] = set.mapAs("{ x: 'x', value: 'value2' }")
            2 -> seriesMapping[2] = set.mapAs("{ x: 'x', value: 'value3' }")
            3 -> seriesMapping[3] = set.mapAs("{ x: 'x', value: 'value4' }")
        }
    }

    val series = arrayOfNulls<Line>(datas.size)

    for (i in datasSeries.indices) {
        series[i] = cartesian.line(seriesMapping[i])
        series[i]?.name(datasSeries[i])
        series[i]?.hovered()?.markers()?.enabled(true)
        series[i]?.hovered()?.markers()
            ?.type(MarkerType.CIRCLE)
            ?.size(4.0)
        series[i]?.tooltip()
            ?.position("right")
            ?.anchor(Anchor.LEFT_CENTER)
            ?.offsetX(5.0)
            ?.offsetY(5.0)
    }

    cartesian.legend().enabled(true)
    cartesian.legend().fontSize(13.0)
    cartesian.legend().padding(0.0, 0.0, 10.0, 0.0)

    this.setChart(cartesian)
}

fun AnyChartView.toVerticalChart(title: String, data: List<DataEntry>) {
    APIlib.getInstance().setActiveAnyChartView(this)

    val vertical = AnyChart.vertical()

    vertical.animation(true)
        .title(title)

    val set = Set.instantiate()
    set.data(data)
    val barData = set.mapAs("{ x: 'x', value: 'value' }")
    val jumpLineData = set.mapAs("{ x: 'x', value: 'jumpLine' }")

    val bar = vertical.bar(barData)
    bar.labels().format("{%Value}")
    bar.labels().fontColor("#1a135c")
    bar.labels().fontSize(12)

    val jumpLine = vertical.jumpLine(jumpLineData)
    jumpLine.stroke("2 #ffbce5")
    jumpLine.labels().enabled(false)

    vertical.yScale().minimum(0)

    vertical.labels(true)
    vertical.labels().fontSize(12)
    vertical.labels().fontColor("#1a135c")

    vertical.tooltip()
        .displayMode(TooltipDisplayMode.UNION)
        .positionMode(TooltipPositionMode.POINT)
        .unionFormat(
            "function() {\n" +
                    "      return 'Plain: $' + this.points[1].value + ' mln' +\n" +
                    "        '\\n' + 'Fact: $' + this.points[0].value + ' mln';\n" +
                    "    }"
        )

    vertical.interactivity().hoverMode(HoverMode.BY_X)

    vertical.xAxis(true)
    vertical.yAxis(true)
    vertical.yAxis(0).labels().format("{%Value}")
    vertical.yAxis(0).labels().fontColor("#1a135c")
    vertical.yAxis(0).labels().fontSize(12)

    this.setChart(vertical)
}

fun AnyChartView.toColumnChart(
    data: List<DataEntry>,
    data1: List<DataEntry>?,
    data2: List<DataEntry>?,
    datas: List<String>?,
    datas1: List<String>?,
    datas2: List<String>?,
    title: String,
    xAxis: String,
    prefix: String?,
    yAxis: String,
    isLabeling: Boolean?
) {
    APIlib.getInstance().setActiveAnyChartView(this)

    val cartesian = AnyChart.column()

    if (datas != null) {
        val set = Set.instantiate()
        set.data(data)
        val seriesData = set.mapAs("{ x: 'x', value: 'value' }")

        val column = cartesian.column(seriesData)
        column.name(datas[0])
        column.hovered().markers().enabled(true)
        column.hovered().markers()
            .type(MarkerType.CIRCLE)
            .size(4.0)

        column.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format(prefix!! + "{%Value}{groupsSeparator: }")

        cartesian.legend().enabled(true)
        cartesian.legend().fontSize(13.0)
        cartesian.legend().padding(0.0, 0.0, 20.0, 0.0)
    } else {
        val column = cartesian.column(data)

        column.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format(prefix!! + "{%Value}{groupsSeparator: }")
    }

    if (datas1 != null) {
        val set1 = Set.instantiate()
        set1.data(data1)

        val seriesData = set1.mapAs("{ x: 'x', value: 'value' }")

        val column = cartesian.column(seriesData)
        column.name(datas!![1])
        column.hovered().markers().enabled(true)
        column.hovered().markers()
            .type(MarkerType.CIRCLE)
            .size(4.0)

        column.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format("$prefix{%Value}{groupsSeparator: }")

        cartesian.legend().enabled(true)
        cartesian.legend().fontSize(13.0)
        cartesian.legend().padding(0.0, 0.0, 20.0, 0.0)
    } else {
        val column1 = cartesian.column(data1)

        column1.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format("$prefix{%Value}{groupsSeparator: }")
    }

    if (datas2 != null) {
        val set1 = Set.instantiate()
        set1.data(data2)

        val seriesData = set1.mapAs("{ x: 'x', value: 'value' }")

        val column = cartesian.column(seriesData)
        column.name(datas!![2])
        column.hovered().markers().enabled(true)
        column.hovered().markers()
            .type(MarkerType.CIRCLE)
            .size(4.0)

        column.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format("$prefix{%Value}{groupsSeparator: }")

        cartesian.legend().enabled(true)
        cartesian.legend().fontSize(13.0)
        cartesian.legend().padding(0.0, 0.0, 20.0, 0.0)
    } else {
        val column1 = cartesian.column(data1)

        column1.tooltip()
            .titleFormat("{%X}")
            .position(Position.CENTER_BOTTOM)
            .anchor(Anchor.CENTER_BOTTOM)
            .offsetX(0.0)
            .offsetY(5.0)
            .format("$prefix{%Value}{groupsSeparator: }")
    }

    cartesian.animation(true)
    cartesian.title(title)

    cartesian.labels(isLabeling)
    cartesian.xAxis(0).labels().padding(5.0, 5.0, 5.0, 5.0)

    cartesian.yScale().minimum(0.0)

    cartesian.yAxis(0).labels().format("$prefix{%Value}{groupsSeparator: }")

    cartesian.tooltip().positionMode(TooltipPositionMode.POINT)
    cartesian.interactivity().hoverMode(HoverMode.BY_X)

    cartesian.xAxis(0).title(xAxis)
    cartesian.yAxis(0).title(yAxis)

    // set the padding between columns
    cartesian.barsPadding(-0.5)

    //  set the padding between column groups
    cartesian.barGroupsPadding(2)

    //set scroll orientation
    cartesian.xScroller(false)
    cartesian.xScroller().enabled(false)
    cartesian.xScroller().orientation("left")

    this.setChart(cartesian)
}

fun AnyChartView.toBarChart(
    yAxis: String,
    title: String,
    seriesData: List<DataEntry>,
    dataOne: String,
    dataTwo: String
) {
    APIlib.getInstance().setActiveAnyChartView(this)

    val barChart = AnyChart.bar()
    barChart.animation(true)
    barChart.padding(10.0, 20.0, 5.0, 20.0)
    barChart.yScale().stackMode(ScaleStackMode.VALUE)
    barChart.yAxis(0).labels().format(
        "function() {\n" +
                "    return Math.abs(this.value).toLocaleString();\n" +
                "  }"
    )
    barChart.yAxis(0.0).title(yAxis)
    barChart.xAxis(0.0).overlapMode(LabelsOverlapMode.ALLOW_OVERLAP)

    val xAxis1 = barChart.xAxis(1.0)
    xAxis1.enabled(true)
    xAxis1.orientation(Orientation.RIGHT)
    xAxis1.overlapMode(LabelsOverlapMode.ALLOW_OVERLAP)

    barChart.title(title)
    barChart.interactivity().hoverMode(HoverMode.BY_X)
    barChart.tooltip()
        .title(false)
        .separator(false)
        .displayMode(TooltipDisplayMode.SEPARATED)
        .positionMode(TooltipPositionMode.POINT)
        .useHtml(true)
        .fontSize(12.0)
        .offsetX(5.0)
        .offsetY(0.0)
        .format(
            ("function() {\n" +
                    "      return '<span style=\"color: #D9D9D9\">Rp</span>' + Math.abs(this.value).toLocaleString();\n"
                    +
                    "    }")
        )

    val set = Set.instantiate()
    set.data(seriesData)
    val series1Data = set.mapAs("{ x: 'x', value: 'value' }")
    val series2Data = set.mapAs("{ x: 'x', value: 'value2' }")

    val series1 = barChart.bar(series1Data)
    series1.name(dataOne)
        .color("HotPink")
    series1.tooltip()
        .position("right")
        .anchor(Anchor.LEFT_CENTER)

    val series2 = barChart.bar(series2Data)
    series2.name(dataTwo)
    series2.tooltip()
        .position("left")
        .anchor(Anchor.RIGHT_CENTER)

    barChart.legend().enabled(true)
    barChart.legend().inverted(true)
    barChart.legend().fontSize(13.0)
    barChart.legend().padding(0.0, 0.0, 20.0, 0.0)

    this.setChart(barChart)
}

fun AnyChartView.toWaterfallChart(
    title: String,
    data: MutableList<DataEntry>
    ) {

    APIlib.getInstance().setActiveAnyChartView(this)

    val waterfall = AnyChart.waterfall()

    waterfall.title(title)

    waterfall.yScale().minimum(0.0)

    waterfall.yAxis(0).labels().format("\${%Value}{scale:(1000)(100)|(mln)}")
    waterfall.labels().enabled(true)
    waterfall.labels().format(
        "function() {\n" +
                "      if (this['isTotal']) {\n" +
                "        return anychart.format.number(this.absolute, {\n" +
                "          scale: true\n" +
                "        })\n" +
                "      }\n" +
                "\n" +
                "      return anychart.format.number(this.value, {\n" +
                "        scale: true\n" +
                "      })\n" +
                "    }"
    )

    val end = DataEntry()
    end.setValue("x", "End")
    end.setValue("isTotal", true)
    data.add(end)

    waterfall.data(data)

    this.setChart(waterfall)
}

fun AnyChartView.toAreaChart(
    title: String,
    seriesData: List<DataEntry>,
    areaOne: String,
    areaTwo: String,
    yAxis: String,
    prefix: String,
    postfix: String
) {

    val areaChart = AnyChart.area()

    areaChart.animation(true)

    val crosshair = areaChart.crosshair()
    crosshair.enabled(true)

    crosshair.yStroke(null as Stroke?, null, null, null as String?, null as String?)
        .xStroke("#fff", 1.0, null, null as String?, null as String?)
        .zIndex(39.0)
    crosshair.yLabel(0).enabled(true)

    areaChart.yScale().stackMode(ScaleStackMode.VALUE)

    areaChart.title(title)

    val set = Set.instantiate()
    set.data(seriesData)

    val series1Data = set.mapAs("{ x: 'x', value: 'value' }")
    val series2Data = set.mapAs("{ x: 'x', value: 'value2' }")

    val series1 = areaChart.area(series1Data)
    series1.name(areaOne)
    series1.stroke("3 #fff")
    series1.hovered().stroke("3 #fff")
    series1.hovered().markers().enabled(true)
    series1.hovered().markers()
        .type(MarkerType.CIRCLE)
        .size(4.0)
        .stroke("1.5 #fff")
    series1.markers().zIndex(100.0)

    val series2 = areaChart.area(series2Data)
    series2.name(areaTwo)
    series2.stroke("3 #fff")
    series2.hovered().stroke("3 #fff")
    series2.hovered().markers().enabled(true)
    series2.hovered().markers()
        .type(MarkerType.CIRCLE)
        .size(4.0)
        .stroke("1.5 #fff")
    series2.markers().zIndex(100.0)

    areaChart.legend().enabled(true)
    areaChart.legend().fontSize(13.0)
    areaChart.legend().padding(0.0, 0.0, 20.0, 0.0)

    areaChart.xAxis(0).title(false)
    areaChart.yAxis(0).title(yAxis)

    areaChart.interactivity().hoverMode(HoverMode.BY_X)
    areaChart.tooltip()
        .valuePrefix(prefix)
        .valuePostfix(postfix)
        .displayMode(TooltipDisplayMode.UNION)

    this.setChart(areaChart)
}