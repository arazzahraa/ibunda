package com.ara.ibunda.utils

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import com.ara.ibunda.R
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.domain.Event
import kotlinx.android.synthetic.main.alert_dialog_calendar_view.view.*
import java.util.*

fun showCustomCalendarViewDialog(context: Context, onDateClicked:((date:Date?)->Unit), events:List<Event>){
    val builder = AlertDialog.Builder(context)
    val view = LayoutInflater.from(context).inflate(R.layout.alert_dialog_calendar_view, null)
    builder.setView(view)
    val dialog = builder.create()
    dialog.show()

    with(view){
        val currentMonth= getMonthAndYear(calendarView.firstDayOfCurrentMonth)
        tvMonth.text = currentMonth

        calendarView.addEvents(events)

        calendarView.setListener(object : CompactCalendarView.CompactCalendarViewListener{
            override fun onDayClick(dateClicked: Date?) {
                onDateClicked.invoke(dateClicked)
                dialog.dismiss()
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date?) {
                val newMonth= getMonthAndYear(firstDayOfNewMonth ?: Date())
                tvMonth.text = newMonth
            }

        })
    }
}