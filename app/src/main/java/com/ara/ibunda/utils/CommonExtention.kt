package com.ara.ibunda.utils

import android.app.Activity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager

fun FragmentActivity.replaceFragment(View:Int, fragment: Fragment, isBackStack:Boolean){
    this.supportFragmentManager
        .beginTransaction()
        .replace(View, fragment)
        .addToBackStack (if (isBackStack) fragment.tag else null)
        .commit()
}

fun Fragment.replaceFragment(View:Int, fragment: Fragment, isBackStack:Boolean){
    this.childFragmentManager
        .beginTransaction()
        .replace(View, fragment)
        .addToBackStack (if (isBackStack) fragment.tag else null)
        .commit()
}