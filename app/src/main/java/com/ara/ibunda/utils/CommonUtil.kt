package com.ara.ibunda.utils

import com.ara.ibunda.CalenderActivity
import com.ara.ibunda.enumtype.RepeaterType
import com.google.firebase.Timestamp
import java.sql.Time
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun getCurrentDate(): String {
    var date = Date()
    var simpleDateFormat = SimpleDateFormat("EEEE,dd MMMM yyyy")
    var currentDate = simpleDateFormat.format(date)
    return currentDate
}

fun getSelectedDate(selectedDateFromUser: Date): String {
    var simpleDateFormat = SimpleDateFormat("EEEE,dd MMMM yyyy")
    var selectedDate = simpleDateFormat.format(selectedDateFromUser)
    return selectedDate
}

fun getSelectedTime(selectedTimeFromUser: Date): String {
    var simpleTimeFormat = SimpleDateFormat("hh:mm")
    var selectedTime = simpleTimeFormat.format(selectedTimeFromUser)
    return selectedTime
}

fun getCurrentTime(): String? {
    var time = System.currentTimeMillis()
    var simpleTimeFormat = SimpleDateFormat("hh:mm")
    var currentTime = simpleTimeFormat.format(time)
    return currentTime
}

fun Date.toFirestoreTimestamp(): Timestamp {
    return Timestamp(this)
}

fun getCurrentFirestoreTimestamp(): Timestamp {
    return Calendar.getInstance().time.toFirestoreTimestamp()
}

fun getMonthFromDate(date: Date):String{
    return android.text.format.DateFormat.format("MMMM", date).toString()
}

fun getDayFromDate(date: Date):String{
    return android.text.format.DateFormat.format("dd", date).toString()
}

fun getFullDate(date: Date):String{
    return android.text.format.DateFormat.format("EEEE,dd MMMM yyyy", date).toString()
}

fun getMonthAndYear(date: Date):String{
    return android.text.format.DateFormat.format("MMMM yyyy", date).toString()
}

fun getDatesUntilEndYear(startDate:Date, repeaterType: Int, endDate:Date? = null) : MutableList<Date>  {
    val datesInRange =  arrayListOf<Date>()
    val calendar = Calendar.getInstance()
    calendar.setTime(startDate)

    val endCalendar = Calendar.getInstance();
//    endCalendar.set(Calendar.MONTH, 5)
//    endCalendar.set(Calendar.DAY_OF_MONTH, 30)
    endDate?.let {
        endCalendar.time = endDate
    }

    val range = when(repeaterType){
        RepeaterType.NO_REPEAT.type -> 0
        RepeaterType.DAILY.type -> 1
        RepeaterType.TWO_DAY.type -> 2
        RepeaterType.WEEKLY.type -> 7
        RepeaterType.TWO_WEEKLY.type -> 14
        else -> 0
    }

    while (calendar.before(endCalendar)) {
        val result = calendar.time
        datesInRange.add(result)
        calendar.add(Calendar.DATE, range)
    }

    return datesInRange;
}

fun convertData(originalDate:String, originalFormat:String, targetFormat:String):String{
    val original = SimpleDateFormat(originalFormat, Locale.US)
    val target = SimpleDateFormat(targetFormat, Locale.US)
    val date = original.parse(originalDate)
    return target.format(date)
}