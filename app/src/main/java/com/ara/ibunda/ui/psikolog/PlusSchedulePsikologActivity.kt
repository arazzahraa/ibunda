package com.ara.ibunda

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import com.ara.ibunda.enumtype.RepeaterType
import com.ara.ibunda.model.SchedulePsikolog
import com.ara.ibunda.model.User
import com.ara.ibunda.utils.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_plus_schedule_psikolog.*
import kotlinx.android.synthetic.main.activity_plus_schedule_psikolog.contentLoadingProgressBar
import java.util.*

class PlusSchedulePsikologActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var startDate: Date?=null
    var endDate: Date?=null
    var listSchedule: MutableList<Date> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_schedule_psikolog)

        setUpSpinnerPengulangan()

        tvDateStartSchedule.text = getCurrentDate()
        tvEndDate.text = getCurrentDate()

        tvTimeStartSchedule.text = getCurrentTime()
        tvTimeEndSchedule.text = getCurrentTime()

        tvDateStartSchedule.setOnClickListener { getSelectedDateFromDatePicker(tvDateStartSchedule, true) }
        tvEndDate.setOnClickListener { getSelectedDateFromDatePicker(tvEndDate, false) }

        tvTimeEndSchedule.setOnClickListener { getSelectedDateFromDatePicker(tvDateStartSchedule, false) }
        tvTimeStartSchedule.setOnClickListener { getSelectedTimeFromTimePicker(tvTimeStartSchedule) }

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        startDate = Date()

        mbsaveSchedulePsikolog.setOnClickListener {
            contentLoadingProgressBar.visibility=View.VISIBLE
            getPsikolog()
        }

    }

    fun getSelectedDateFromDatePicker(textView: TextView, isStartSchedule: Boolean) {
        var calender = Calendar.getInstance()
        var selectedYear = calender.get(Calendar.YEAR)
        var selectedMonth = calender.get(Calendar.MONTH)
        var selectedDay = calender.get(Calendar.DATE)

        var datePickerDialog: DatePickerDialog = DatePickerDialog(
            this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                calender.set(Calendar.YEAR, year)
                calender.set(Calendar.MONTH, month)
                calender.set(Calendar.DATE, dayOfMonth)
                val selectedDate = calender.time
                val selectedDateInStrig = getSelectedDate(selectedDate)

                if (isStartSchedule){
                    startDate= calender.time
                }else{
                    endDate = calender.time
                }

                textView.setText(selectedDateInStrig)
            }, selectedYear, selectedMonth, selectedDay
        )
        datePickerDialog.datePicker.minDate=System.currentTimeMillis()-1000
        datePickerDialog.show()
    }

    fun getSelectedTimeFromTimePicker(textView: TextView) {
        var time = Calendar.getInstance()
        var selectedHour = time.get(Calendar.HOUR)
        var selectedMinute = time.get(Calendar.MINUTE)

        var limitStartHour = 10
        var limitEndHour = 21

        var timePickerDialog = TimePickerDialog(
            this, TimePickerDialog.OnTimeSetListener { view, hour, minute ->
                time.set(Calendar.HOUR, hour)
                time.set(Calendar.MINUTE, minute)
                val selectedTime = time.time
                val selectedTimeInString = getSelectedTime(selectedTime)
                time.add(Calendar.HOUR,1)
                val timeEnd = time.time
                val timeEndInString = getSelectedTime(timeEnd)

                if (hour<limitStartHour || hour>limitEndHour){
                    Toast.makeText(
                        this,
                        "Silahkan pilih jam lain",
                        Toast.LENGTH_SHORT
                    ).show()
                    mbsaveSchedulePsikolog.isEnabled = false
                }
                else{
                    textView.setText(selectedTimeInString)
                    tvTimeEndSchedule.setText(timeEndInString)

                    mbsaveSchedulePsikolog.isEnabled = true
                }

            }, selectedHour, selectedMinute, true
        )

        timePickerDialog.show()
    }

    fun setUpSpinnerPengulangan() {
        var gender: MutableList<String> = mutableListOf("Pengulangan" , "Setiap hari" , "Setiap dua hari", "Setiap minggu", "Setiap dua minggu")
        var repeatAdapter= ArrayAdapter(this,android.R.layout.simple_spinner_item,gender)
        repeatAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerPenggulangan.adapter = repeatAdapter
    }

    fun getPsikolog(){
        var idPsikolog = firebaseAuthentication?.currentUser?.uid.orEmpty()
        firebaseFireStore?.collection("User")?.document(idPsikolog)
            ?.get()
            ?.addOnSuccessListener {
                val psikolog = it.toObject(User::class.java)

                startDate?.let {startDate->
                    listSchedule.clear()
                    val selectedRepeater = spinnerPenggulangan.selectedItemPosition
                    if (selectedRepeater != RepeaterType.NO_REPEAT.type){
                        listSchedule = getDatesUntilEndYear(startDate, selectedRepeater, endDate)
                    }else{
                        listSchedule.add(startDate)
                    }

                    listSchedule.forEachIndexed { index, date ->
                        doSaveSchedulePsikolog(psikolog ?: User(), date, index)
                    }
                }
            }
    }

    fun doSaveSchedulePsikolog(psikolog:User, date:Date, index:Int){
        var scheduleContent= SchedulePsikolog(
            timeStamp = date.toFirestoreTimestamp(),
            timeStart = tvTimeStartSchedule.text.toString(),
            timeEnd = tvTimeEndSchedule.text.toString(),
            repeat = spinnerPenggulangan.selectedItem.toString(),
            isAvailable = true,
            idPsikolog = psikolog.id,
            typePsikolog = psikolog.typePsikolog
        )

        firebaseFireStore?.collection("SchedulePsikolog")
            ?.add(scheduleContent)
            ?.addOnSuccessListener {
                var idSchedulePsikolog= it.id
                firebaseFireStore?.collection("SchedulePsikolog")
                    ?.document(idSchedulePsikolog)
                    ?.update("id", idSchedulePsikolog)
                    ?.addOnSuccessListener {
                        if (index == listSchedule.size - 1){
                            resetPage()
                            Toast.makeText(
                                this,
                                "Berhasil Menambahkan Jadwal, silahkan lihat jadwal pada halaman List jadwal",
                                Toast.LENGTH_SHORT
                            ).show()
                            contentLoadingProgressBar.visibility=View.GONE
                        }
                    }
                    ?.addOnFailureListener {
                        Log.d("save schedule", "error $index ${it.message}")
                        if (index == listSchedule.size - 1){
                            Toast.makeText(this, "Gagal menyimpan jadwal ${it.message}", Toast.LENGTH_SHORT).show()
                            contentLoadingProgressBar.visibility=View.GONE
                        }
                    }
            }
            ?.addOnFailureListener {
                Toast.makeText(this, "Gagal menyimpan jadwal ${it.message}", Toast.LENGTH_SHORT).show()

            }
    }

    fun resetPage(){
        tvDateStartSchedule.setText(getCurrentDate())
        tvEndDate.setText(getCurrentDate())
        tvTimeStartSchedule.setText(getCurrentTime())
        tvTimeEndSchedule.setText(getCurrentTime())

        spinnerPenggulangan.setSelection(0)

    }

}
