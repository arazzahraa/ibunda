package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.ListSchedulePsikologAdapter
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_list_schedule.*
import kotlinx.android.synthetic.main.activity_list_schedule.contentLoadingProgressBar
import kotlinx.android.synthetic.main.activity_plus_schedule_psikolog.*
import kotlinx.android.synthetic.main.fragment_chat_psikolog.*
import kotlinx.android.synthetic.main.fragment_profile.*

class ListScheduleActivity : AppCompatActivity() {

    val listSchedulePsikologAdapter: ListSchedulePsikologAdapter by lazy {
        ListSchedulePsikologAdapter(
            this,
            mutableListOf(),
            {})
    }
    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_schedule)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        getScheduleData()


    }

    fun setUpListSchedulePsikolog(listSchedulePsikolog: MutableList<ListSchedulePsikolog>) {
        listSchedulePsikologAdapter.datas.addAll(listSchedulePsikolog)
        rvListSchedulePsikolog.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listSchedulePsikologAdapter
        }
    }


    fun getScheduleData() {
        contentLoadingProgressBar.visibility = View.VISIBLE

        var idPsikolog = firebaseAuthentication?.currentUser?.uid

        firebaseFireStore?.collection("SchedulePsikolog")
            ?.whereEqualTo("idPsikolog", idPsikolog)
            ?.orderBy("timeStamp", Query.Direction.ASCENDING)
            ?.get()
            ?.addOnSuccessListener {
                val schedulePsikolog = it.toObjects(SchedulePsikolog::class.java)
                val scheduleByMonth = schedulePsikolog.distinctBy { it.getScheduleMonth() }
                val listMonth = schedulePsikolog.map { it.getScheduleMonth() }.distinct()
                val listSchedule = mutableListOf<ListSchedulePsikolog>()

                listMonth.forEach { month ->
                    val scheduleContent =
                        schedulePsikolog.filter { it.getScheduleMonth() == month }.toMutableList()
                    val schedule = ListSchedulePsikolog(
                        month = month,
                        content = scheduleContent
                    )
                    listSchedule.add(schedule)
                }

                setUpListSchedulePsikolog(listSchedule)
                contentLoadingProgressBar.visibility = View.GONE
            }

            ?.addOnFailureListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT).show()
            }

    }
}


