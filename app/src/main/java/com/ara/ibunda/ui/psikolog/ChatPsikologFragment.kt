package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.ChatAdapter
import com.ara.ibunda.adapter.KonselingAdapter
import com.ara.ibunda.model.Chat
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.Patient
import com.ara.ibunda.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.fragment_chat_psikolog.*

/**
 * A simple [Fragment] subclass.
 */
class ChatPsikologFragment : Fragment() {

    var firebaseFirestore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null

    var listKonseling: MutableList<User> = mutableListOf()
    var listIdUser:List<String> = listOf()
    var konseling: MutableList<Konseling> = mutableListOf()


    private val patientAdapter: KonselingAdapter by lazy {
        KonselingAdapter(
            context = context!!,
            datas = mutableListOf(),
            listener = { konseling ->
                val intent = Intent(requireContext(), ChatActivity::class.java)
                intent.putExtra("Konseling", konseling)
                startActivity(intent)
            },
            rekamKasusListener = {konseling->
                val intent = Intent(requireContext(), RekamKasusActivity::class.java)
                intent.putExtra("Konseling", konseling)
                startActivity(intent)
            })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat_psikolog, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contentLoadingProgressBarChatPsikolog.visibility= View.VISIBLE

        firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        getKonseling()
    }

    fun setUpPatientList(konseling: MutableList<Konseling>) {
        patientAdapter.datas.addAll(konseling)
        rvPsikolog.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = patientAdapter
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }

    }

    fun getKonseling() {
        val psikolog = firebaseAuthentication?.currentUser?.uid
        firebaseFirestore?.collection("Konseling")
            ?.whereEqualTo("idPsikolog", psikolog)
            ?.get()
            ?.addOnSuccessListener {
                contentLoadingProgressBarChatPsikolog.visibility= View.GONE
                konseling = it.toObjects(Konseling::class.java)
                setUpPatientList(konseling)

//                listIdUser = konseling.map { it.idUser }
//                listIdUser.forEachIndexed { index, s ->
//                    getUser(s, index)
                }
            }
    }

