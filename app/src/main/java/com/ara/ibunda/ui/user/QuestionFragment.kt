package com.ara.ibunda

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.AnswerAdapter
import com.ara.ibunda.model.Answer
import com.ara.ibunda.model.Question
import kotlinx.android.synthetic.main.fragment_question.*


class QuestionFragment : Fragment() {

    companion object{
        fun newInstance(question: Question, page: Int):QuestionFragment{
            val fragment= QuestionFragment()
            val bundle= Bundle()
            bundle.putParcelable("question", question)
            bundle.putInt("page",page)
            fragment.arguments=bundle
            return fragment
        }
    }

    var answer: MutableList<Answer> = mutableListOf()
    var question: Question?=null
    var page: Int=0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_question, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        question= arguments?.getParcelable("question")
        page= arguments?.getInt("page", page) ?: 1
        tvPage.text=(page).toString()
        tvquestion.text= question?.title
        answer= question?.answer?.toMutableList() ?: mutableListOf()
        setUpAnswerList(answer)
    }

    fun setUpAnswerList(answer: MutableList<Answer>) {
        var answerAdapter = context?.let { AnswerAdapter(it, answer) }
        rvanswer.apply {
            layoutManager=LinearLayoutManager(context)
            adapter=answerAdapter

        }

    }


}
