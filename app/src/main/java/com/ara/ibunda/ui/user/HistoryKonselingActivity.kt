package com.ara.ibunda.ui.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.ChatActivity
import com.ara.ibunda.R
import com.ara.ibunda.adapter.KonselingAdapter
import com.ara.ibunda.adapter.PsikologAdapter
import com.ara.ibunda.adapter.PsikologKonselingAdapter
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.Psikolog
import com.ara.ibunda.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_history_konseling.*

class HistoryKonselingActivity : AppCompatActivity() {

    var firebaseFirestore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null

    var konseling: MutableList<Konseling> = mutableListOf()

    private val patientAdapter: PsikologKonselingAdapter by lazy {
        PsikologKonselingAdapter(
            context = this,
            datas = mutableListOf(),
            listener = { konseling ->
                val intent = Intent(this, ChatActivity::class.java)
                intent.putExtra("Konseling", konseling)
                startActivity(intent)
            }
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_konseling)

        firebaseFirestore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        getKonseling()
    }

    fun setUpPsikologList(konseling: MutableList<Konseling>) {
        patientAdapter.datas.addAll(konseling)
        rvKonseling.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = patientAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@HistoryKonselingActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }

    }

    fun getKonseling() {
        val user = firebaseAuthentication?.currentUser?.uid
        firebaseFirestore?.collection("Konseling")
            ?.whereEqualTo("idUser", user)
            ?.get()
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility= View.GONE
                konseling = it.toObjects(Konseling::class.java)
                setUpPsikologList(konseling)
            }
    }
}
