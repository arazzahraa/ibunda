package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.utils.replaceFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_psikolog_menu.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class PsikologMenuActivity : AppCompatActivity(),
    BottomNavigationView.OnNavigationItemSelectedListener {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null
    var preference: IbundaPreference? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_psikolog_menu)
        mainMenuPsikolog.setOnNavigationItemSelectedListener(this)
        replaceFragment(R.id.frameLayout, ChatPsikologFragment(), false)

        tvSchedulePsikolog.setOnClickListener {
            replaceFragment(R.id.frameLayout, JadwalPsikologFragment(), false)
        }

        preference = IbundaPreference(this)
        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        setSupportActionBar(toolbar)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.ChatPsikolog -> {
                replaceFragment(R.id.frameLayout, ChatPsikologFragment(), false)
                return true
            }

            R.id.JadwalPsikolog -> {
                replaceFragment(R.id.frameLayout, JadwalPsikologFragment(), false)
                return true
            }

            R.id.ProfilePsikolog -> {
                replaceFragment(R.id.frameLayout, ProfileFragment(), false)
                return true
            }
        }
        return false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_logout, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.keluar_psikolog_admin -> {
                firebaseAuthentication?.signOut()
                preference?.saveBoolean("IS_LOGGED_IN", false)
                preference?.saveString("USER_TYPE", "")
                val intent = Intent(this, SignupActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }
        return true
    }
}
