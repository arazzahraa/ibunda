package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActiity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen_actiity)

        GlobalScope.launch { delay(3000)
        val intent = Intent(this@SplashScreenActiity, SignupActivity::class.java )
            startActivity(intent)

        }
    }
}
