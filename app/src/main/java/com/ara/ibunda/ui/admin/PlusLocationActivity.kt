package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ara.ibunda.adapter.LocationAdapter
import com.ara.ibunda.model.Location
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_plus_location.*

class PlusLocationActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_location)

        setUpSpinnerKota()

        mbSavePsikolog.setOnClickListener {
            if (validate()) {
                doSaveDataLocation()
            } else {
                Toast.makeText(
                    this,
                    "Silahkan lengkapi data lokasi konseling",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        firebaseFireStore = FirebaseFirestore.getInstance()

//        mbEditLokasi.setOnClickListener {
//            val intent= Intent (this, EditLokasiFragment::class.java)
//            startActivity(intent)
//        }
    }


    fun setUpSpinnerKota() {
        var gender: MutableList<String> = mutableListOf("Kota" , "Jakarta" , "Bandung", "Surabaya", "Bali")
        var locationAdapter= ArrayAdapter(this,android.R.layout.simple_spinner_item,gender)
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerIsiNmaKota.adapter = locationAdapter
    }

    fun doSaveDataLocation(){

        var location= Location(
            city =spinnerIsiNmaKota.selectedItem.toString(),
            name= tiIsiNamaLokasi.text.toString(),
            address = tiIsiAlamatLokasi.text.toString()
        )

        firebaseFireStore?.collection("Location")
            ?.add(location)
            ?.addOnSuccessListener {
                var idLocation= it.id
                firebaseFireStore?.collection("Location")
                    ?.document(idLocation)
                    ?.update("id", idLocation)
                    ?.addOnSuccessListener {
                        resetPage()
                        Toast.makeText(
                            this,
                            "Berhasil Menambahkan Lokasi Konseling",
                            Toast.LENGTH_SHORT
                        ).show()  }
                    ?.addOnFailureListener {
                        Toast.makeText(this, "Gagal menyimpan lokasi ${it.message}", Toast.LENGTH_SHORT).show()}
            }
            ?.addOnFailureListener {
                Toast.makeText(this, "Gagal menyimpan lokasi ${it.message}", Toast.LENGTH_SHORT).show()

            }
    }

    fun resetPage(){
        tiIsiNamaLokasi.setText("")
        tiIsiAlamatLokasi.setText("")

        spinnerIsiNmaKota.setSelection(0)

    }

    fun validate(): Boolean {

        var namaKota = spinnerIsiNmaKota.isSelected.toString()
        var namaLokasi = tiIsiNamaLokasi.text.toString()
        var alamatLokasi = tiIsiAlamatLokasi.text.toString()


        return (!namaKota.isEmpty() &&
                !namaLokasi.isEmpty() &&
                !alamatLokasi.isEmpty())
    }


}
