package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.MainMenuType
import kotlinx.android.synthetic.main.activity_chat_infomation.*

class ChatInfomationActivity : AppCompatActivity() {

    var type: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_infomation)

        type =intent.getIntExtra("curhat",CurhatType.CURHAT.type)

        setUpSpinnerPsikologType()

        mbNextCoun.setOnClickListener {
            if(validate()){
                when(type){
                    CurhatType.CURHAT.type->{
                        val intent= Intent(this, CalenderActivity::class.java)
                        intent.putExtra("curhat", type)
                        startActivity(intent)
                    }
                    CurhatType.ECOUNSELING.type->{
                        val intent= Intent(this, PsikologPickerActivity::class.java)
                        intent.putExtra("curhat", type)
                        intent.putExtra("typePsikolog", typeCounseling.selectedItem.toString())
                        startActivity(intent)
                    }
                    CurhatType.COUNSELINGCORNER.type->{
                        val intent= Intent(this, LocationActivity::class.java)
                        intent.putExtra("location", MainMenuType.COUNSELINGCORNER.type)
                        intent.putExtra("typePsikolog", typeCounseling.selectedItem.toString())
                        startActivity(intent)
                    }
                }
            }
            else{
                Toast.makeText(this, "Silahkan isi type konseling", Toast.LENGTH_SHORT).show()
            }
        }

        when(type){
            CurhatType.CURHAT.type->{
                typeCounseling.visibility = View.GONE
                mbNextCoun.text="Next"
            }
            CurhatType.ECOUNSELING.type->{
                tvTitle.text= "E-counseling"
                tvDescription.text= getString(R.string.description_ecounseling)
            }
            CurhatType.COUNSELINGCORNER.type->{
                tvTitle.text= "Counseling Courner"
                tvDescription.text= getString(R.string.description_counseling_corner)
                btnLihatLokasi.visibility= View.GONE
                btnLihatLokasi.setOnClickListener {
                    val intent = Intent (this, LocationActivity::class.java)
                    startActivity(intent)
                }
            }
        }

    }

    fun setUpSpinnerPsikologType() {

        when (type){

            CurhatType.ECOUNSELING.type->{
                var type: MutableList<String> = mutableListOf("Pilih type konseling" , "Essential" , "Premium")
                var typeCounselingAdapter= ArrayAdapter(this,android.R.layout.simple_spinner_item,type)
                typeCounselingAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                typeCounseling.adapter = typeCounselingAdapter
            }

            CurhatType.COUNSELINGCORNER.type->{
                var type: MutableList<String> = mutableListOf("Pilih type konseling" , "Basic", "Essential" , "Profesional","Premium")
                var typeCounselingAdapter= ArrayAdapter(this,android.R.layout.simple_spinner_item,type)
                typeCounselingAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                typeCounseling.adapter = typeCounselingAdapter
            }
        }

    }

    fun validate(): Boolean {

        var selectedTypeConseling = typeCounseling.selectedItemPosition

        return (selectedTypeConseling != 0)
    }


}
