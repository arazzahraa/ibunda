package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.LocationAdapter
import com.ara.ibunda.adapter.PsikologAdapter
import com.ara.ibunda.model.Location
import com.ara.ibunda.model.Psikolog
import com.ara.ibunda.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_location.*
import kotlinx.android.synthetic.main.activity_psikolog_picker.*
import kotlinx.android.synthetic.main.fragment_chat.*

/**
 * A simple [Fragment] subclass.
 */
class ChatFragment : Fragment() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        btnChat.setOnClickListener {
            val intent = Intent (context, ChatServiceActivity::class.java)
            startActivity(intent)
        }

        getPsikolog()
        getLocation()
    }

    fun getPsikolog(){
        firebaseFireStore?.collection("User")
            ?.whereEqualTo("status", "Psikolog")
            ?.get()
            ?.addOnSuccessListener {
                var psikolog = it.toObjects(User::class.java).take(2).toMutableList()
                setUpPsikologList(psikolog)
            }
    }

    fun setUpPsikologList(psikolog: MutableList<User>) {
        var psikologAdapter = PsikologAdapter(requireContext(), psikolog.map { it.toPsikolog() }.toMutableList()) {}

        rvPsiko.apply {
            layoutManager= LinearLayoutManager(requireContext())
            adapter=psikologAdapter
            addItemDecoration(
                DividerItemDecoration(requireContext(),
                    DividerItemDecoration.VERTICAL)
            )
        }
    }

    fun getLocation(){
        firebaseFireStore?.collection("Location")
            ?.get()
            ?.addOnSuccessListener {
                var location = it.toObjects(Location::class.java).take(2).toMutableList()
                setUpLocationList(location)
            }
    }

    fun setUpLocationList(location: MutableList<Location>) {
        var locationAdapter = LocationAdapter(requireContext(), location, {})

        rvLoc.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = locationAdapter
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }


}
