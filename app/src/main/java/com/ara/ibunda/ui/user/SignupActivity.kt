package com.ara.ibunda

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.Slide
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.adapter.SliderPagerAdapter
import com.ara.ibunda.utils.getSelectedDate
import kotlinx.android.synthetic.main.activity_calender.*
import kotlinx.android.synthetic.main.activity_location.*
import kotlinx.android.synthetic.main.activity_signup.*
import java.util.*

class SignupActivity : AppCompatActivity() {

    var fragment: MutableList<Fragment> = mutableListOf()
    var pager: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        setUpViewPager()

    }

    fun setUpViewPager() {
        fragment = mutableListOf(
            SignupFragment(), LoginFragment()
        )
        pager = mutableListOf(
            "Daftar","Masuk"
        )

        var adapter= SliderPagerAdapter(supportFragmentManager,fragment,pager)

        vpSignup.adapter=adapter
        tlSignup.setupWithViewPager(vpSignup)
    }
}

