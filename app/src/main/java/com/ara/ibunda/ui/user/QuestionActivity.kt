package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import com.ara.ibunda.enumtype.CalenderType
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.QuestionType
import com.ara.ibunda.model.Answer
import com.ara.ibunda.model.Question
import com.ara.ibunda.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_calender.*
import kotlinx.android.synthetic.main.activity_question.*

class QuestionActivity : AppCompatActivity() {

    var question: MutableList<Question> = mutableListOf()
    var answerOne: MutableList<Answer> = mutableListOf()
    var answerTwo: MutableList<Answer> = mutableListOf()
    var answerThree: MutableList<Answer> = mutableListOf()
    var page: Int = 1
    lateinit var currentQuestion: Question

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_question)
        val type = intent.getIntExtra("curhat", CurhatType.ECOUNSELING.type)
        var calenderType = CalenderType.CURHAT.type
        when(type){
            CurhatType.CURHAT.type->{calenderType=CalenderType.CURHAT.type}
            CurhatType.ECOUNSELING.type->{calenderType=CalenderType.ECOUNSELING.type}
            CurhatType.COUNSELINGCORNER.type->{calenderType=CalenderType.COUNSELINGCORNER.type}

        }

        answerOne = mutableListOf(
            Answer("Masalah Diri"),
            Answer("Pertemanan"),
            Answer("Keluarga")
        )

        answerTwo = mutableListOf(
            Answer("Perempuan"),
            Answer("Laki-laki")
        )

        answerThree = mutableListOf(
            Answer("Islam"),
            Answer("Kristen"),
            Answer("Budha")
        )

        question = mutableListOf(
            Question("Masalah seperti apa yang ingin kamu bicarakan dengan bunda?", answerOne),
            Question("Kamu lebih nyaman cerita dengan psikolog dengan gender tertentu?", answerTwo),
            Question("Kamu lebih nyaman cerita dengan psikolog dengan agama tertentu?", answerThree)
        )

        currentQuestion = question.first()
        replaceFragment(
            R.id.frameLayout,
            QuestionFragment.newInstance(currentQuestion, page),
            false
        )

        btnPsikolog.setOnClickListener(){
            val intent = Intent (this,PsikologPickerActivity::class.java)
            startActivity(intent)
        }
        

        btnNext.setOnClickListener {
            page++

            if (page > question.size) {
                val intent = Intent(this, CalenderActivity::class.java)
                intent.putExtra("curhat", type)
                intent.putExtra("calender", calenderType)
                startActivity(intent)
            } else {
                currentQuestion = question.get(page - 1)
                replaceFragment(
                    R.id.frameLayout,
                    QuestionFragment.newInstance(currentQuestion, page),
                    true
                )
            }
        }
    }

//    override fun onBackPressed() {
//        page--
//        if(page<1){
//            finish()
//        }
//        else{
//            currentQuestion = question.get(page - 1)
//            replaceFragment(
//                R.id.frameLayout,
//                QuestionFragment.newInstance(currentQuestion, page),
//                true
//            )
//        }
//        super.onBackPressed()

//    }

}


