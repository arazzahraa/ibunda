package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ara.ibunda.enumtype.CalenderType
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.MainMenuType
import kotlinx.android.synthetic.main.activity_chat_service.*

class ChatServiceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_service)

        curhat.setOnClickListener {
            val intent = Intent (this, ChatInfomationActivity::class.java)
            intent.putExtra("curhat", CurhatType.CURHAT.type)
            intent.putExtra("calender", CalenderType.CURHAT.type)
            startActivity(intent)
        }

        ecounseling.setOnClickListener {
            val intent = Intent (this, ChatInfomationActivity::class.java)
            intent.putExtra("curhat", CurhatType.ECOUNSELING.type)
            startActivity(intent)
        }

        counselingcorner.setOnClickListener {
            val intent = Intent (this, ChatInfomationActivity::class.java)
            intent.putExtra("location", MainMenuType.COUNSELINGCORNER.type)
            intent.putExtra("curhat", CurhatType.COUNSELINGCORNER.type)
            startActivity(intent)
        }

    }
}
