package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.ui.admin.RekapitulasiKonselingActivity
import com.ara.ibunda.utils.replaceFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main_menu_admin.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MainMenuAdminActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null
    var preference: IbundaPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu_admin)
        mainMenuPsikolog.setOnNavigationItemSelectedListener(this)
        replaceFragment(R.id.frameLayoutAdmin, ConfirmAdminFragment(), false)

        preference = IbundaPreference(this)
        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        setSupportActionBar(toolbar)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.ConfirmAdmin -> {
                replaceFragment(R.id.frameLayoutAdmin, ConfirmAdminFragment(), false)
                return true
            }

            R.id.TambahAdmin -> {
                replaceFragment(R.id.frameLayoutAdmin, PlusAdminFragment(), false)
                return true
            }

            R.id.ProfileAdmin -> {
                replaceFragment(R.id.frameLayoutAdmin, ProfileFragment(), false)
                return true
            }
        }
        return false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_logout_admin, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.rekapitulasi_konseling -> {
                val intent = Intent(this, RekapitulasiKonselingActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }

            R.id.keluar_admin -> {
                firebaseAuthentication?.signOut()
                preference?.saveBoolean("IS_LOGGED_IN", false)
                preference?.saveString("USER_TYPE", "")
                val intent = Intent(this, SignupActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }
        return true
    }
}

