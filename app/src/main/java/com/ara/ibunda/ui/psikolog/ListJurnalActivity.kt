package com.ara.ibunda.ui.psikolog

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.JurnalFragment
import com.ara.ibunda.R
import com.ara.ibunda.adapter.JurnalAdapter
import com.ara.ibunda.model.Jurnal
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_list_jurnal.*

class ListJurnalActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var jurnal: Jurnal? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_jurnal)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        val idUser= intent.getStringExtra("user")
        idUser?.let {
            getJurnal(idUser = it)
        }

    }

    fun getJurnal(idUser: String) {
        firebaseFireStore?.collection("Jurnal")
            ?.whereEqualTo("idUser", idUser)
            ?.get()
            ?.addOnSuccessListener {
                var jurnal = it.toObjects(Jurnal::class.java)
                setUpJurnalList(jurnal)
            }
    }

    fun setUpJurnalList(jurnal: MutableList<Jurnal>) {
        rvJurnal.apply {
            var jurnalAdapter = JurnalAdapter(this@ListJurnalActivity, jurnal) { jurnal ->
                val intent = Intent (this@ListJurnalActivity, JurnalPsikologAcvtivity::class.java)
                intent.putExtra("Jurnal", jurnal)
                startActivity(intent)
            }
            layoutManager = LinearLayoutManager(this@ListJurnalActivity)
            adapter = jurnalAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@ListJurnalActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }
}
