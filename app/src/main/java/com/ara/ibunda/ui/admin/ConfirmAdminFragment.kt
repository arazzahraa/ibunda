package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.ConfirmPaymentAdapter
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.ConfirmPayment
import com.ara.ibunda.model.Payment
import com.ara.ibunda.model.User
import com.ara.ibunda.utils.replaceFragment
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_detail_payment.*
import kotlinx.android.synthetic.main.fragment_chat_psikolog.*
import kotlinx.android.synthetic.main.fragment_confirm_admin.*

/**
 * A simple [Fragment] subclass.
 */
class ConfirmAdminFragment : Fragment() {

    var firebaseFirestore: FirebaseFirestore? = null
    var listUser: MutableList<User> = mutableListOf()

    var listPembayaran: MutableList<Payment> = mutableListOf()

    private val confirmPaymentAdapter: ConfirmPaymentAdapter by lazy {
        ConfirmPaymentAdapter(context!!, mutableListOf()) {
            val intent = Intent(requireContext(), DetailPaymentActivity::class.java)
            intent.putExtra("detail pembayaran", it)
            startActivity(intent)
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm_admin, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contentLoadingProgressBarAdmin.visibility = View.VISIBLE

        firebaseFirestore = FirebaseFirestore.getInstance()

        getPembayaran()
    }

//    override fun onResume() {
//        getPembayaran()
//        super.onResume()
//    }

    fun setUpConfirmPayList(confirmPayment: MutableList<ConfirmPayment>) {
        confirmPaymentAdapter.datas.clear()
        confirmPaymentAdapter.datas.addAll(confirmPayment)
        rvConfirmPay.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = confirmPaymentAdapter
            addItemDecoration(
                DividerItemDecoration(
                    requireContext(),
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }

    fun getPembayaran() {
        firebaseFirestore?.collection("Pembayaran")
            ?.whereEqualTo("paymentStatus", PaymentStatusType.MENUNGGUKONFIRMASI.type)
            ?.get()
            ?.addOnSuccessListener {
                var pembayaran = it.toObjects(Payment::class.java)
                var listIdUser: List<String> = pembayaran.map { it.idUser }
                listPembayaran= pembayaran


                if (pembayaran.isEmpty()) {
                    contentLoadingProgressBarAdmin.visibility = View.GONE
                }
                else {
                    listIdUser.forEachIndexed { index, s ->
                        getUser(s, index)
                    }
                }

            }
            ?.addOnFailureListener {
                contentLoadingProgressBarAdmin.visibility = View.GONE

                Toast.makeText(
                    requireContext(),
                    " ${it?.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

    fun getUser(idUser: String, index: Int) {
        firebaseFirestore?.collection("User")?.document(idUser)
            ?.get()
            ?.addOnSuccessListener {
                var user = it.toObject(User::class.java)
                listUser.add(
                    user ?: User(
                    )
                )

                if(index== listPembayaran.size-1){
                    setUpConfirmPayList(listUser.map { it.toConfirmPayment() }.toMutableList())
                    contentLoadingProgressBarAdmin.visibility = View.GONE
                }
            }
            ?.addOnFailureListener {
                contentLoadingProgressBarAdmin.visibility = View.GONE

                Toast.makeText(
                    requireContext(),
                    " ${it?.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }
}

