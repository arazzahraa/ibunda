package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_plus_admin.*

/**
 * A simple [Fragment] subclass.
 */
class PlusAdminFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_plus_admin, container, false)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ivPsi.setOnClickListener {
            val intent = Intent(context, PlusPsikologActivity::class.java)
            startActivity(intent)
        }

        ivLoc.setOnClickListener {
            val intent = Intent(context, PlusLocationActivity::class.java)
            startActivity(intent)
        }
    }
}

