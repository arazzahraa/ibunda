package com.ara.ibunda.ui.psikolog

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager

import com.ara.ibunda.R
import com.ara.ibunda.adapter.SchedulePsikologAdapter
import com.ara.ibunda.model.SchedulePsikolog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.fragment_schedule_psikolog_detail.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SchedulePsikologDetail.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SchedulePsikologDetail.newInstance] factory method to
 * create an instance of this fragment.
 */
class SchedulePsikologDetailFragment : BottomSheetDialogFragment() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_schedule_psikolog_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listSchedulePsikolog = arguments?.getParcelableArrayList<SchedulePsikolog>("detail schedule")

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        if (listSchedulePsikolog != null) {
            showSchedule(listSchedulePsikolog.toMutableList())
        }
    }



    fun showSchedule(schedulePsikolg: MutableList<SchedulePsikolog>){
        val schedulePsikologAdapter = SchedulePsikologAdapter(requireContext(),schedulePsikolg,{})
        rvDetailSchedule.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = schedulePsikologAdapter
        }
    }

}
