package com.ara.ibunda


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.core.widget.addTextChangedListener
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.User
import com.ara.ibunda.utils.replaceFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_chat_service.*
import kotlinx.android.synthetic.main.activity_plus_psikolog.*
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.activity_upload_payment_receipt.*
import kotlinx.android.synthetic.main.fragment_chat.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.spinnerGender
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.fragment_signup.*
import java.io.File
import com.ara.ibunda.SignupActivity as SignupActivity1

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseStorage: FirebaseStorage? = null
    var storageReference: StorageReference? = null

    var isUpdate = false
    var isUpdateAut = false
    var user: User? = null

    var path: String = ""


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }


    fun setUpSpinnerGender() {
        var gender: MutableList<String> = mutableListOf("Jenis Kelamin", "Perempuan", "Laki-laki")
        var genderAdapter =
            ArrayAdapter(requireContext(), R.layout.item_border_spinner_signup, gender)
        genderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGender.adapter = genderAdapter
    }

    fun setUpSpinnerTypePsi() {
        var typePsi: MutableList<String> =
            mutableListOf("Jenis Psikolog","Konselor", "Basic", "Essential", "Profesional", "Premium")
        var genderAdapter =
            ArrayAdapter(requireContext(), R.layout.item_border_spinner_signup, typePsi)
        genderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerPsikolog.adapter = genderAdapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        contentLoadingProgressBarProfil.visibility = View.VISIBLE

        setUpSpinnerGender()
        setUpSpinnerTypePsi()

        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()
        storageReference = firebaseStorage?.getReference()?.child("avatar")

        getProfileData()
//        changed()

        mbChangeDp.setOnClickListener {
            setUpImage()
        }


        mbKeluar.setOnClickListener {
            contentLoadingProgressBarProfil.visibility = View.VISIBLE
            doSaveAvatar(path)
        }

    }

    fun doSaveProfil(imgURL: String){
        val idUser = firebaseAuthentication?.currentUser?.uid.orEmpty()
        val user = User(
            id = idUser,
            email = etEmail.text.toString(),
            namaLengkap = etNama.text.toString(),
            password = etPassword.text.toString(),
            gender = spinnerGender.selectedItem.toString(),
            age = etAge.text.toString(),
            typePsikolog = spinnerPsikolog.selectedItem.toString(),
            domisili = user?.domisili.orEmpty(),
            typeAdmin = etAdmin.text.toString(),
            pengalaman = user?.pengalaman.orEmpty(),
            status = user?.status.orEmpty(),
            avatar = imgURL

        )

        firebaseFireStore?.collection("User")?.document(idUser)
            ?.set(user)
            ?.addOnSuccessListener {
                if (isUpdateAut) {
                    changeAut()
                }
                Toast.makeText(
                    requireContext(),
                    "Berhasil menyimpan profil",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarProfil.visibility = View.GONE
            }
            ?.addOnFailureListener {
                Toast.makeText(
                    requireContext(),
                    "Berhasil menyimpan profil",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarProfil.visibility = View.GONE
            }
    }

//    fun changed() {
//        etEmail.doOnTextChanged { text, start, count, after ->
//            isUpdateAut = true
//            isUpdate = true
//            mbKeluar.text = if (isUpdate) "Simpan" else "Keluar"
//        }
//        etPassword.doOnTextChanged { text, start, count, after ->
//            isUpdateAut = true
//            isUpdate = true
//            mbKeluar.text = if (isUpdate) "Simpan" else "Keluar"
//        }
//        etNama.doOnTextChanged { text, start, count, after ->
//            isUpdate = true
//            mbKeluar.text = if (isUpdate) "Simpan" else "Keluar"
//        }
//        etAdmin.doOnTextChanged { text, start, count, after ->
//            isUpdate = true
//            mbKeluar.text = if (isUpdate) "Simpan" else "Keluar"
//        }
//    }

    fun changeAut() {
        val user = firebaseAuthentication?.currentUser
        user?.updatePassword(etPassword.text.toString())
        user?.updateEmail(etEmail.text.toString())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            var image: Image = ImagePicker.getFirstImageOrNull(data)
            path = image.path
            contentLoadingProgressBarAvatar.visibility = View.VISIBLE
            Glide.with(this).load(path).listener(object :RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    contentLoadingProgressBarAvatar.visibility = View.GONE
                    avatar.setImageResource(R.drawable.ic_profile)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    contentLoadingProgressBarAvatar.visibility = View.GONE
                    return false
                }


            }).into(avatar)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun setUpImage() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.ALL) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start(); // start image picker activity with request code
    }

    fun getProfileData() {
        contentLoadingProgressBarAvatar.visibility = View.VISIBLE
        val idUser = firebaseAuthentication?.currentUser?.uid
        idUser?.let {
            firebaseFireStore?.collection("User")?.document(it)
                ?.get()
                ?.addOnSuccessListener {
                    user = it.toObject(User::class.java)

                    when (user?.status) {
                        UserType.USER.type -> {
                            spinnerPsikolog.visibility = View.GONE
                            tvTypePsi.visibility = View.GONE
                            tvAdmin.visibility = View.GONE
                            etAdmin.visibility = View.GONE
                            appCompatImageView18.visibility = View.GONE
                            appCompatImageView19.visibility = View.GONE
                        }
                        UserType.PSIKOLOG.type -> {
                            tvAdmin.visibility = View.GONE
                            etAdmin.visibility = View.GONE
                            appCompatImageView19.visibility = View.GONE

                        }
                        UserType.ADMIN.type -> {
                            spinnerPsikolog.visibility = View.GONE
                            tvTypePsi.visibility = View.GONE
                            appCompatImageView18.visibility = View.GONE

                        }
                    }

                    var requestOption= RequestOptions()
                        .placeholder(R.drawable.ic_profile)
                        .error(R.drawable.ic_profile)
                    Glide.with(this).load(user?.avatar).listener(object: RequestListener<Drawable>{
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            contentLoadingProgressBarAvatar.visibility = View.GONE
                            avatar.setImageResource(R.drawable.ic_profile)
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            contentLoadingProgressBarAvatar.visibility = View.GONE
                            return false
                        }

                    }).apply(requestOption).into(avatar)
                    etEmail.setText(user?.email.orEmpty())
                    etPassword.setText(user?.password.orEmpty())
                    etNama.setText(user?.namaLengkap.orEmpty())
                    etAdmin.setText(user?.typeAdmin.orEmpty())
                    when (user?.gender) {
                        "Perempuan" -> spinnerGender.setSelection(1)
                        "Laki-laki" -> spinnerGender.setSelection(2)
                    }
                    etAge.setText(user?.age.orEmpty())
                    when (user?.typePsikolog) {
                        "Konselor" -> spinnerPsikolog.setSelection(1)
                        "Basic" -> spinnerPsikolog.setSelection(2)
                        "Essential" -> spinnerPsikolog.setSelection(3)
                        "Profesional" -> spinnerPsikolog.setSelection(4)
                        "Premium" -> spinnerPsikolog.setSelection(5)
                    }
                    contentLoadingProgressBarProfil.visibility = View.GONE
                }
        }
    }

    fun doSaveAvatar(imagePath: String){
        var file= Uri.fromFile(File(imagePath))
        var document = storageReference?.child("${file.lastPathSegment}")

        var uploadTask= document?.putFile(file)

        uploadTask?.continueWithTask {task->
            if(!task.isSuccessful){
                task.exception?.let {
                    throw it
                }
            }
            document?.downloadUrl
        }?.addOnCompleteListener { task ->
            contentLoadingProgressBarProfil.visibility = View.GONE
            if (task.isSuccessful) {
                val downloadUrl = task.result.toString()
                doSaveProfil(downloadUrl)
            } else {
                Toast.makeText(
                    requireContext(),
                    "Gagal menyimpan perubahan profil${task.exception?.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
            ?.addOnFailureListener {
                contentLoadingProgressBarProfil.visibility = View.GONE
                Toast.makeText(
                    requireContext(),
                    "Gagal menyimpan perubahan profil",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }

}
