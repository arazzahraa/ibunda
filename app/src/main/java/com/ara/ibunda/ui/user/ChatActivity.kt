package com.ara.ibunda

import android.content.Intent
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.adapter.ChatAdapter
import com.ara.ibunda.adapter.PsikologAdapter
import com.ara.ibunda.enumtype.JurnalType
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.Chat
import com.ara.ibunda.model.ChatRoom
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.User
import com.ara.ibunda.ui.psikolog.JurnalPsikologAcvtivity
import com.ara.ibunda.ui.psikolog.ListJurnalActivity
import com.ara.ibunda.ui.psikolog.ListRekamKasusActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_chat.*
import kotlinx.android.synthetic.main.activity_chat.contentLoadingProgressBar
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class ChatActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null

    val chatAdapter: ChatAdapter by lazy { ChatAdapter (this, mutableListOf()) }

    var konseling:Konseling? =null

    private var sender: User? = null

    private var currentUserId = ""

    private var preference:IbundaPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        setSupportActionBar(linearLayout2)

        var chat: MutableList<Chat> = mutableListOf()

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()
        preference = IbundaPreference(this)

        currentUserId = firebaseAuthentication?.currentUser?.uid.orEmpty()

        konseling = intent.getParcelableExtra("Konseling")

        ivSendChat.setOnClickListener{
            contentLoadingProgressBar.visibility = View.VISIBLE
            sendChat()
        }

        getChats()

        val senderId = if (currentUserId == konseling?.idUser) konseling?.idPsikolog else konseling?.idUser
        getSender(senderId.orEmpty())
    }

    private fun getSender(idSender:String){
        firebaseFireStore?.collection("User")?.document(idSender)
            ?.get()
            ?.addOnSuccessListener {
                sender = it.toObject(User::class.java)
                showOpponentProfileSummary()
            }
    }

    private fun showOpponentProfileSummary() {
        sender?.let { user ->
            var requestOption = RequestOptions()
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
            Glide.with(this).load(user.avatar).listener(object:
                RequestListener<Drawable> {
                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    dataSource: com.bumptech.glide.load.DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: com.bumptech.glide.request.target.Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    ivDisplayPic.setImageResource(R.drawable.ic_profile)
                    return false
                }

            }).apply(requestOption).into(ivDisplayPic)
            tvNamaChat.text = user.namaLengkap
        }
    }

    private fun sendChat() {
        val chat = Chat(
            idChatRoom = konseling?.id.orEmpty(),
            idSender = firebaseAuthentication?.currentUser?.uid.orEmpty(),
            chat = etChat.text.toString())

        firebaseFireStore?.collection("Chat")
            ?.add(chat)
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                getChats()
                resetChat()
            }
            ?.addOnFailureListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(this, "${it.message}", Toast.LENGTH_SHORT)
                    .show()
            }
//        pbChat.visible()

    }

    private fun getChats() {
        konseling?.let{
            firebaseFireStore?.collection("Chat")
                ?.whereEqualTo("idChatRoom", it.id)
                ?.orderBy("createdAt", Query.Direction.ASCENDING)
                ?.get()
                ?.addOnSuccessListener {
                    contentLoadingProgressBar.visibility = View.GONE
                    var chat= it.toObjects(Chat::class.java)
                    showChats(chat)
                }
                ?.addOnFailureListener {
                    contentLoadingProgressBar.visibility = View.GONE
                    Log.d("Error Chat", it.message)
                }
        }
    }

    private fun showChats(listOfChat: List<Chat>) {
        chatAdapter.addOrUpdate(listOfChat)
        chatAdapter.currentUserId = firebaseAuthentication?.currentUser?.uid.orEmpty()

        rvChat.apply {
            layoutManager = LinearLayoutManager(this@ChatActivity)
            adapter = chatAdapter
        }
        rvChat.smoothScrollToPosition(chatAdapter.datas.size)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val userStatus = preference?.getString("USER_TYPE").orEmpty()
        if (userStatus == UserType.PSIKOLOG.type){
            menuInflater.inflate(R.menu.main_menu_chat_psikolog,menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.JurnalPasien->{
                val intent= Intent(this, ListJurnalActivity::class.java)
                intent.putExtra("user", sender?.id)
                startActivity(intent)
            }
            R.id.RekamKasusTerdahulu->{
                val intent=Intent(this, ListRekamKasusActivity::class.java)
                intent.putExtra("Konseling", konseling)
                startActivity(intent)
            }
        }
        return true
    }

    private fun resetChat() {
        etChat.setText("")
    }



}
