package com.ara.ibunda

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.ConfirmPayment
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.Payment
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_detail_payment.*
import kotlinx.android.synthetic.main.activity_detail_payment.contentLoadingProgressBar

class DetailPaymentActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null

    var detailPayment: ConfirmPayment? = null

    var konseling: Konseling? = null
    var payment: Payment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_payment)

        contentLoadingProgressBar.visibility = View.VISIBLE

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        detailPayment = intent.getParcelableExtra("detail pembayaran")

        getKonseling()

        mbConfirm.setOnClickListener {
            contentLoadingProgressBar.visibility = View.VISIBLE
            updateKonseling(
                idKonseling = konseling?.id.orEmpty(),
                idPembayaran = payment?.id.orEmpty(),
                idSchedule = konseling?.idSchedule.orEmpty(),
                namePatient = konseling?.patientName.orEmpty()
            )
        }

        mbReject.setOnClickListener {
            contentLoadingProgressBar.visibility = View.VISIBLE
            updateKonselingReject(
                idKonseling = konseling?.id.orEmpty(),
                idPembayaran = payment?.id.orEmpty(),
                idSchedule = konseling?.idSchedule.orEmpty()
            )
        }

    }

    fun getKonseling() {
        firebaseFireStore?.collection("Konseling")
            ?.whereEqualTo("idUser", detailPayment?.id)
            ?.whereEqualTo("paymentStatus", PaymentStatusType.MENUNGGUKONFIRMASI.type)
            ?.get()
            ?.addOnSuccessListener {
                konseling = it.toObjects(Konseling::class.java).firstOrNull()

                etIsiNama.setText(detailPayment?.name.orEmpty())
                etIsiTypeServ.setText(
                    when (konseling?.service) {
                        CurhatType.ECOUNSELING.type -> "E-Counseling"
                        CurhatType.COUNSELINGCORNER.type -> "Counseling Corner"
                        else -> {
                            "nn"
                        }
                    }
                )
                etTypeKon.setText(konseling?.typePsikolog.orEmpty())
                etIsiPrice.setText((konseling?.price.orEmpty()))
                getPaymentData(idKonseling = konseling?.id.orEmpty())

            }

    }


    fun getPaymentData(idKonseling: String) {
        firebaseFireStore?.collection("Pembayaran")
            ?.whereEqualTo("idKonseling", idKonseling)
            ?.get()
            ?.addOnSuccessListener {
                payment = it.toObjects(Payment::class.java).firstOrNull()
                val recieptURL = payment?.paymentReceipt
                Glide.with(this).load(recieptURL).listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        contentLoadingProgressBar.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        contentLoadingProgressBar.visibility = View.GONE
                        return false
                    }
                })
                    .into(ivBuktiBayar)
            }
    }

    fun updateKonseling(
        idKonseling: String,
        idPembayaran: String,
        idSchedule: String,
        namePatient: String
    ) {
        firebaseFireStore?.collection("Konseling")?.document(idKonseling)
            ?.update("paymentStatus", PaymentStatusType.BAYAR.type)
            ?.addOnSuccessListener {
                updateSchedule(idSchedule, idPembayaran, namePatient)
            }
    }

    fun updateKonselingReject(idKonseling: String, idPembayaran: String, idSchedule: String) {
        firebaseFireStore?.collection("Konseling")?.document(idKonseling)
            ?.update("paymentStatus", PaymentStatusType.REJECT.type)
            ?.addOnSuccessListener {
                updatePaymentReject(idPembayaran)
            }
    }

    fun updateSchedule(idSchedule: String, idPembayaran: String, namePatient: String) {
        val updateNewSchedule = mutableMapOf(
            Pair("available", false),
            Pair("namePatient", namePatient)
        )
        firebaseFireStore?.collection("SchedulePsikolog")?.document(idSchedule)
            ?.update(updateNewSchedule)
            ?.addOnSuccessListener {
                updatePayment(idPembayaran)
            }
    }

    fun updatePayment(idPembayaran: String) {
        firebaseFireStore?.collection("Pembayaran")?.document(idPembayaran)
            ?.update("paymentStatus", PaymentStatusType.BAYAR.type)
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(this, "Berhasil mengkonfirmasi", Toast.LENGTH_SHORT).show()
                finish()
            }
    }

    fun updatePaymentReject(idPembayaran: String) {
        firebaseFireStore?.collection("Pembayaran")?.document(idPembayaran)
            ?.update("paymentStatus", PaymentStatusType.REJECT.type)
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(this, "Berhasil menolak pembayaran", Toast.LENGTH_SHORT).show()
                finish()
            }
    }

}

