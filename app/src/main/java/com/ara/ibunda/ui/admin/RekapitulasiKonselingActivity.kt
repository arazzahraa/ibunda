package com.ara.ibunda.ui.admin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.chart.common.dataentry.ValueDataEntry
import com.ara.ibunda.R
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.RekamKasus
import com.ara.ibunda.utils.toColumnChart
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_rekapitulasi_konseling.*

class RekapitulasiKonselingActivity : AppCompatActivity() {

    private var firebaseFirestore:FirebaseFirestore? = null

    private var listKonselingSelesai = mutableListOf<Konseling>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rekapitulasi_konseling)

        firebaseFirestore = FirebaseFirestore.getInstance()

        setUpSpinnerPeriodeWaktu()

        getKonseling()
    }

    fun setUpSpinnerPeriodeWaktu() {
        var periodeWaktu: MutableList<String> = mutableListOf("Periode Waktu", "Perhari", "Perminggu", "Perbulan", "Pertahun")
        var periodeWaktuAdapter =
            ArrayAdapter(this, R.layout.item_border_spinner_signup, periodeWaktu)
        periodeWaktuAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spnWaktuRekap.adapter = periodeWaktuAdapter
    }

    private fun getKonseling(){
        pbChart.visibility = View.VISIBLE
        firebaseFirestore?.collection("Konseling")
            ?.whereEqualTo("paymentStatus", PaymentStatusType.SELESAI.type)
            ?.get()
            ?.addOnSuccessListener {
                pbChart.visibility = View.GONE
                listKonselingSelesai = it.toObjects(Konseling::class.java)

                var hashMapKonselingService = hashMapOf<String, List<Konseling>>()

                hashMapKonselingService = listKonselingSelesai.groupBy {
                    it.getMonthKonseling()
                } as HashMap<String, List<Konseling>>

                val konselingKeys = ArrayList<String>(hashMapKonselingService.keys)
                val konselingCurhatData:ArrayList<DataEntry> = arrayListOf()
                val konselingECounselingData:ArrayList<DataEntry> = arrayListOf()
                val konselingCounselingCornerData:ArrayList<DataEntry> = arrayListOf()

                konselingKeys.forEach { currentMonth->
                    var totalCurhat = hashMapKonselingService[currentMonth]?.filter { it.service == CurhatType.CURHAT.type}?.size
                    var totalEcounseling = hashMapKonselingService[currentMonth]?.filter { it.service == CurhatType.ECOUNSELING.type}?.size
                    var totalCouncelingCorner = hashMapKonselingService[currentMonth]?.filter { it.service == CurhatType.COUNSELINGCORNER.type}?.size

                    konselingCurhatData.add(ValueDataEntry(currentMonth, totalCurhat))
                    konselingECounselingData.add(ValueDataEntry(currentMonth, totalEcounseling))
                    konselingCounselingCornerData.add(ValueDataEntry(currentMonth, totalCouncelingCorner))

                    Log.d("Anychart success"," data $hashMapKonselingService $currentMonth curhat $totalCurhat, ecounceling $totalEcounseling, concelingcorene $totalCouncelingCorner")
                }

                val series = mutableListOf("Curhat", "ECounceling", "Counceling Corner")

                chartRecap.toColumnChart(
                    datas = series,
                    datas1 = arrayListOf(),
                    datas2 = arrayListOf(),
                    data = konselingCurhatData,
                    data1 = konselingECounselingData,
                    data2 = konselingCounselingCornerData,
                    title = "2020",
                    xAxis = "",
                    yAxis = "",
                    isLabeling = false,
                    prefix = ""
                )

            }
            ?.addOnFailureListener {
                Log.d("Anychart Error", it.message.toString())
            }
    }
}
