package com.ara.ibunda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_sand_k.*

class SAndKActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sand_k)

        showSnk()
    }

    private fun showSnk(){
        wvSnk.loadUrl("https://www.ibunda.id/info")
    }
}
