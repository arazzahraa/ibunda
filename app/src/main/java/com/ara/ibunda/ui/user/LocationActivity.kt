package com.ara.ibunda

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.LocationAdapter
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.MainMenuType
import com.ara.ibunda.model.Location
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_location.*

class LocationActivity : AppCompatActivity() {

    var location: MutableList<Location> = mutableListOf()
    var firebaseFireStore : FirebaseFirestore? = null
    var typePsikolog : String =""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        typePsikolog= intent.getStringExtra("typePsikolog").orEmpty()


        firebaseFireStore = FirebaseFirestore.getInstance()

        getLocation(typePsikolog)

        spinLocation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var selectedLocation = parent?.selectedItem.toString()
                var sugestedLocation = location.filter { it.city == selectedLocation }
                setUpLocationList(sugestedLocation.toMutableList())
            }
        }
    }

    fun getLocation(type : String){
        firebaseFireStore?.collection("Location")
            ?.get()
            ?.addOnSuccessListener {
                location = it.toObjects(Location::class.java)

                setUpSpinnerLocation (location.map { it.city }.distinct().toMutableList())

            }
    }

    fun setUpSpinnerLocation(listLocation: MutableList<String>) {
        var locationAdapter = ArrayAdapter(this,android.R.layout.simple_spinner_item, listLocation)
        locationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinLocation.adapter = locationAdapter
    }

    fun setUpLocationList(location: MutableList<Location>) {
        val type = intent.getIntExtra("location", MainMenuType.INFORMATION.type)
        var locationAdapter = LocationAdapter(this, location) {
            when (type) {
                MainMenuType.INFORMATION.type -> {
                    Toast.makeText(this, "Clicked", Toast.LENGTH_SHORT).show()
                }
                MainMenuType.COUNSELINGCORNER.type -> {
                    val intent = Intent(this, PsikologPickerActivity::class.java)
                    intent.putExtra("curhat", CurhatType.COUNSELINGCORNER.type)
                    intent.putExtra("selected city", it.city)
                    intent.putExtra("selected address", it.address)
                    intent.putExtra("typePsikolog", typePsikolog)
                    startActivity(intent)
                }
            }
        }

        rvaddres.apply {
            layoutManager = LinearLayoutManager(this@LocationActivity)
            adapter = locationAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@LocationActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }
}
