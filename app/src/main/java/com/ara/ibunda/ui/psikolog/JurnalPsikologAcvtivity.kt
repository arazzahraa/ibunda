package com.ara.ibunda.ui.psikolog

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ara.ibunda.JurnalFragment
import com.ara.ibunda.R
import com.ara.ibunda.model.Jurnal
import com.ara.ibunda.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_jurnal_psikolog_acvtivity.*

class JurnalPsikologAcvtivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jurnal_psikolog_acvtivity)

        val jurnal = intent.getParcelableExtra<Jurnal>("Jurnal")
        val jurnalFragment= JurnalFragment()
        val bundle= Bundle()
        bundle.putBoolean("isPsikolog", true)
        bundle.putParcelable("Jurnal", jurnal)
        jurnalFragment.arguments=bundle

        replaceFragment(R.id.flJurnal, jurnalFragment, true)
    }

}
