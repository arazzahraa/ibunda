package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_login.*

/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment() {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null
    var preference:IbundaPreference? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mbLogin.setOnClickListener {
            contentLoadingProgressBarLogIn.visibility=View.VISIBLE
            if (validate()) {
                doLogIn()
            } else {
                Toast.makeText(
                    requireContext(),
                    "Silahkan isi email dan password anda",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarLogIn.visibility=View.GONE
            }
        }

        preference = IbundaPreference(requireContext())
        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        if (preference?.getBoolean("IS_LOGGED_IN") == true){
            val status = preference?.getString("USER_TYPE") ?: UserType.USER.type
            toNextActivity(status)
        }

    }

    fun doLogIn() {

        var email = etEmailSignin.text.toString()
        var password = etPasswordSignin.text.toString()

        firebaseAuthentication?.signInWithEmailAndPassword(email, password)
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    val idUserLogin = it.result?.user?.uid
                    getUserData(idUserLogin.orEmpty())
                    contentLoadingProgressBarLogIn.visibility=View.GONE
                } else Toast.makeText(
                    requireContext(),
                    "Failed Sign In ${it.exception?.message}",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarLogIn.visibility=View.GONE
            }
    }

    fun validate(): Boolean {

        var email = etEmailSignin.text.toString()
        var password = etPasswordSignin.text.toString()

        return (!email.isEmpty() && !password.isEmpty())
    }

    fun getUserData(idUser: String) {
        firebaseFireStore?.collection("User")?.document(idUser)?.get()
            ?.addOnSuccessListener {document->
                val user = document.toObject(User::class.java)
                preference?.saveString("USER_NAME", user?.namaLengkap.orEmpty())
                preference?.saveString("USER_AVATAR", user?.avatar.orEmpty())
                user?.status?.let { toNextActivity(it) }
            }

    }

    fun toNextActivity(status: String) {
        preference?.saveBoolean("IS_LOGGED_IN", true)
        preference?.saveString("USER_TYPE", status)
        when (status) {
            UserType.USER.type -> {
                val intent = Intent(requireContext(), MainActivity::class.java)
                startActivity(intent)
                activity?.finish()
            }
            UserType.PSIKOLOG.type -> {
                val intent = Intent(requireContext(), PsikologMenuActivity::class.java)
                startActivity(intent)
                activity?.finish()

            }
            UserType.ADMIN.type -> {
                val intent = Intent(requireContext(), MainMenuAdminActivity::class.java)
                startActivity(intent)
                activity?.finish()

            }
        }
    }
}
