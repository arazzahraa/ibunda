package com.ara.ibunda.ui.psikolog

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.R
import com.ara.ibunda.RekamKasusActivity
import com.ara.ibunda.adapter.RekamKasusAdapter
import com.ara.ibunda.enumtype.RekamKasusType
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.RekamKasus
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_list_rekam_kasus_activity.*

class ListRekamKasusActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var konseling:Konseling? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_rekam_kasus_activity)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        konseling = intent.getParcelableExtra<Konseling>("Konseling")

        konseling?.let {
            getRekamKasus(it.idUser)
        }
    }

    fun getRekamKasus(idUser: String) {
        firebaseFireStore?.collection("RekamKasus")
            ?.whereEqualTo("idUser", idUser)
            ?.get()
            ?.addOnSuccessListener {
                var rekamKasus = it.toObjects(RekamKasus::class.java)
                setUpRekamKasusList(rekamKasus)
            }
    }

    fun setUpRekamKasusList(rekamKasus: MutableList<RekamKasus>) {
        rvRekamKasus.apply {
            var rekamKasusAdapter = RekamKasusAdapter(this@ListRekamKasusActivity, rekamKasus) {rekamKasus->
                val intent = Intent(this@ListRekamKasusActivity, RekamKasusActivity::class.java)
                intent.putExtra("rekam kasus", RekamKasusType.LIHAT.type)
                intent.putExtra("Konseling", konseling)
                intent.putExtra("rekam kasus data", rekamKasus)
                startActivity(intent)
            }
            layoutManager = LinearLayoutManager(this@ListRekamKasusActivity)
            adapter = rekamKasusAdapter
            addItemDecoration(
                DividerItemDecoration(
                    this@ListRekamKasusActivity,
                    DividerItemDecoration.VERTICAL
                )
            )
        }
    }
}
