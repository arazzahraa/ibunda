package com.ara.ibunda


import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import android.widget.Toast
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.User
import com.ara.ibunda.utils.getSelectedDate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_calender.*
import kotlinx.android.synthetic.main.activity_location.*
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_signup.*
import kotlinx.android.synthetic.main.fragment_signup.contentLoadingProgressBar
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class SignupFragment : Fragment() {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_signup, container, false)


    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mbSignup.setOnClickListener{
            if (validate()) {
                contentLoadingProgressBar.visibility=View.VISIBLE
                doSignUp(etEmailUser.text.toString(), etPaswordUser.text.toString())}
            else {
                Toast.makeText(
                    requireContext(),
                    "Silahkan lengkapi data diri anda",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


        setUpSpinnerGender()

        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        etAgeUser.setOnClickListener { getSelectedDateFromDatePicker() }


    }

    fun getSelectedDateFromDatePicker() {
        var calender = Calendar.getInstance()
        var selectedYear = calender.get(Calendar.YEAR)
        var selectedMonth = calender.get(Calendar.MONTH)
        var selectedDay = calender.get(Calendar.DATE)

        var datePickerDialog: DatePickerDialog = DatePickerDialog(
            requireContext(), DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                calender.set(Calendar.YEAR, year)
                calender.set(Calendar.MONTH, month)
                calender.set(Calendar.DATE, dayOfMonth)
                val selectedDate = calender.time
                val selectedDateInStrig = getSelectedDate(selectedDate)

                etAgeUser.setText(selectedDateInStrig)
            }, selectedYear, selectedMonth, selectedDay
        )
        datePickerDialog.show()
    }

    fun doSignUp(email: String,  password: String){
        firebaseAuthentication?.createUserWithEmailAndPassword(email,password)
            ?.addOnCompleteListener {
                if(it.isSuccessful){
                    var idUser= firebaseAuthentication?.currentUser?.uid
                    var user= User(
                        id=idUser.orEmpty(),
                        email=etEmailUser.text.toString(),
                        namaLengkap = etNamaLengkapUser.text.toString(),
                        password = etPaswordUser.text.toString(),
                        gender = spinnerGender.selectedItem.toString(),
                        age = etAgeUser.text.toString(),
                        status = UserType.USER.type
                        )
                    doSaveDataUser(user)
                }
                else Toast.makeText(requireContext(), "Failed Sign Up ${it.exception?.message}", Toast.LENGTH_SHORT).show()

            }
    }

    fun doSaveDataUser(user: User){
        firebaseFireStore?.collection("User")?.document(user.id)
            ?.set(user)
            ?.addOnSuccessListener {
                var intent = Intent (context, MainActivity::class.java)
                startActivity(intent)
                activity?.finish()
                contentLoadingProgressBar.visibility=View.GONE
            }
            ?.addOnFailureListener {
                Toast.makeText(requireContext(), "Failed Sign Up ${it.message}", Toast.LENGTH_SHORT).show()
                contentLoadingProgressBar.visibility=View.GONE
            }


    }

    fun setUpSpinnerGender() {
        var gender: MutableList<String> = mutableListOf("Jenis Kelamin" , "Perempuan" , "Laki-laki")
        var genderAdapter= ArrayAdapter(requireContext(),R.layout.item_border_spinner_signup,gender)
        genderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGender.adapter = genderAdapter
    }

    fun validate(): Boolean {

        var email = etEmailUser.text.toString()
        var namaLengkap = etNamaLengkapUser.text.toString()
        var password = etPaswordUser.text.toString()
        var jenisKelamin = spinnerGender.isSelected.toString()
        var umur = etAgeUser.isSelected.toString()

        return (!email.isEmpty() &&
                !namaLengkap.isEmpty() &&
                !password.isEmpty() &&
                !jenisKelamin.isEmpty() &&
                !umur.isEmpty())
    }

}
