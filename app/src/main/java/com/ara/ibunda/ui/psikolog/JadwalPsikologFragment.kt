package com.ara.ibunda


import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ara.ibunda.model.SchedulePsikolog
import com.ara.ibunda.ui.psikolog.SchedulePsikologDetailFragment
import com.ara.ibunda.utils.getMonthAndYear
import com.ara.ibunda.utils.getMonthFromDate
import com.ara.ibunda.utils.getSelectedDate
import com.github.sundeepk.compactcalendarview.CompactCalendarView
import com.github.sundeepk.compactcalendarview.domain.Event
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_jadwal_psikolog.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class JadwalPsikologFragment : Fragment() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var listSchedulePsikolog:MutableList<SchedulePsikolog> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jadwal_psikolog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        getSchedulePsikolog()

        mbListSchedulePsikolog.setOnClickListener{
            val intent = Intent ( context, ListScheduleActivity::class.java)
            startActivity(intent)
        }

        mbPlusSchedulePsikolog.setOnClickListener {
            val intent = Intent ( context, PlusSchedulePsikologActivity::class.java)
            startActivity(intent)
        }

        val currentMonth= getMonthAndYear(calendarView.firstDayOfCurrentMonth)
        tvMonth.text = currentMonth

        calendarView.setListener(object : CompactCalendarView.CompactCalendarViewListener{
            override fun onDayClick(dateClicked: Date?) {
                val selectedDateInString = dateClicked?.let { getSelectedDate(it) }.orEmpty()

                val selectedListSchedulePsikolog = listSchedulePsikolog.filter { it.getFullDate() == selectedDateInString}

                Log.d("Jadwal Psikolog", "date $selectedDateInString, list $selectedListSchedulePsikolog")

                val detailScheduleFragment = SchedulePsikologDetailFragment()
                val bundle = Bundle()
                bundle.putParcelableArrayList("detail schedule", selectedListSchedulePsikolog as ArrayList<out Parcelable>)
                detailScheduleFragment.arguments= bundle
                if(!detailScheduleFragment.isAdded){
                    detailScheduleFragment.show(childFragmentManager, detailScheduleFragment.tag)
                }
            }

            override fun onMonthScroll(firstDayOfNewMonth: Date?) {
                val newMonth= getMonthAndYear(firstDayOfNewMonth ?: Date())
                tvMonth.text = newMonth
            }

        })

    }

    fun getSchedulePsikolog(){
        val idPsikolog = firebaseAuthentication?.currentUser?.uid
        firebaseFireStore?.collection("SchedulePsikolog")
            ?.whereEqualTo("idPsikolog", idPsikolog)
            ?.get()
            ?.addOnSuccessListener {
               listSchedulePsikolog = it.toObjects(SchedulePsikolog::class.java)

                val currentDate = listSchedulePsikolog.map { it.getScheduleDate() }

                val events = arrayListOf<Event>()
                currentDate.forEach {
                    val calendar = Calendar.getInstance()
                    calendar.time = it
                    events.add(Event(R.color.colorAccent, it.time, "ada data"))
                }

                calendarView.addEvents(events)
                pbSchedule.visibility = View.GONE

            }
    }

}
