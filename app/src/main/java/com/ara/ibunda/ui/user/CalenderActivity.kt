package com.ara.ibunda

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.ara.ibunda.enumtype.CalenderType
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.model.Psikolog
import com.ara.ibunda.model.SchedulePsikolog
import com.ara.ibunda.utils.*
import com.github.sundeepk.compactcalendarview.domain.Event
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_calender.*
import java.util.*

class CalenderActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var selectedDatee=""
    var selectedHour=""
    var typeChatService : Int = 0
    var selectedPsikolog : Psikolog? = null
    var selectedCity : String=""
    var selectedAddress : String=""
    var jam = mutableListOf<SchedulePsikolog>()
    val events = arrayListOf<Event>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calender)
//        tvDate.text = getCurrentDate()

        firebaseFireStore = FirebaseFirestore.getInstance()

        tvDate.setOnClickListener { getSelectedDateFromDatePicker() }

        getSpinnerSelectedHour()

        typeChatService = intent.getIntExtra("curhat", CurhatType.CURHAT.type)
        selectedPsikolog = intent.getParcelableExtra("selected psikolog")
        selectedCity = intent.getStringExtra("selected city").orEmpty()
        selectedAddress = intent.getStringExtra("selected address").orEmpty()
        val type = intent.getIntExtra("calender", CalenderType.JURNAL.type)


        mbSaveTime.setOnClickListener {
                if(validate()){
                    when (typeChatService) {
                        CurhatType.CURHAT.type -> {
                            val intent = Intent(this, SummaryActivity::class.java)
                            intent.putExtra("curhat", typeChatService)
                            intent.putExtra("tgl terpilih", selectedDatee)
                            intent.putExtra("jam terpilih", selectedHour)
                            startActivity(intent)
                        }
                        CurhatType.ECOUNSELING.type -> {
                            val intent = Intent(this, SummaryActivity::class.java)
                            intent.putExtra("curhat", CurhatType.ECOUNSELING.type)
                            intent.putExtra("tgl terpilih", selectedDatee)
                            intent.putExtra("jam terpilih", selectedHour)
                            intent.putExtra(("selected psikolog"), selectedPsikolog)
                            startActivity(intent)
                        }
                        CurhatType.COUNSELINGCORNER.type -> {
                            val intent = Intent(this, SummaryActivity::class.java)
                            intent.putExtra("tgl terpilih", selectedDatee)
                            intent.putExtra("jam terpilih", selectedHour)
                            intent.putExtra("curhat", CurhatType.COUNSELINGCORNER.type)
                            intent.putExtra(("selected psikolog"), selectedPsikolog)
                            intent.putExtra("selected city", selectedCity)
                            intent.putExtra("selected address", selectedAddress)
                            startActivity(intent)
                        }
                    }
                }
            else{
                    Toast.makeText(this, "Silahkan lengkapi waktu konseling", Toast.LENGTH_SHORT).show()
                }
        }


        when (type) {
            CalenderType.JURNAL.type -> {
            }
            CalenderType.CURHAT.type, CalenderType.ECOUNSELING.type, CalenderType.COUNSELINGCORNER.type -> {
                tvTitleCalender.text = "Kapan kamu ingin curhat sama bunda?"
                tvDesccCalender.text = "Pilih tanggalnya dibawah sini"
            }
        }
    }

//    fun getSelectedDateFromDatePicker() {
//        showCustomCalendarViewDialog(
//            context = this,
//            onDateClicked = {selectedDate->
//                val fullDateSelected = getFullDate(selectedDate ?: Date())
//                val schedule = scheduleList.find { it.getFullDate() == fullDateSelected }
//                schedule?.let {
//
//                    tvDate.setText(fullDateSelected)
//
//                    getSchedulePsikolog(fullDateSelected)
//
//                    selectedDatee = fullDateSelected
//                }
//            },
//            events = events
//        )
//    }

    fun getSelectedDateFromDatePicker() {
        var calender = Calendar.getInstance()
        var selectedYear = calender.get(Calendar.YEAR)
        var selectedMonth = calender.get(Calendar.MONTH)
        var selectedDay = calender.get(Calendar.DATE)

        var datePickerDialog: DatePickerDialog = DatePickerDialog(
            this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                calender.set(Calendar.YEAR, year)
                calender.set(Calendar.MONTH, month)
                calender.set(Calendar.DATE, dayOfMonth)
                val selectedDate = calender.time
                val selectedDateInStrig = getSelectedDate(selectedDate)

                tvDate.setText(selectedDateInStrig)

                getSchedulePsikolog(selectedDateInStrig)

                selectedDatee = selectedDateInStrig

            }, selectedYear, selectedMonth, selectedDay
        )
        datePickerDialog.show()
    }



    fun setUpSpinnerJam(listJam: MutableList<String>) {
        var jamAdapter= ArrayAdapter(this,android.R.layout.simple_spinner_item, listJam)
        jamAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerJam.adapter = jamAdapter
    }

    fun getSpinnerSelectedHour(){
        spinnerJam.onItemSelectedListener=object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                selectedHour=parent?.selectedItem.toString()
                tvTimeAkhir.setText(jam.find { it.timeStart== selectedHour }?.timeEnd)
            }

        }
    }

    fun getSchedulePsikolog(selectedDate: String){
        firebaseFireStore?.collection("SchedulePsikolog")
            ?.get()
            ?.addOnSuccessListener {
                jam= it.toObjects(SchedulePsikolog::class.java)
                var listJam= listOf<String>()


                when(typeChatService){
                    CurhatType.CURHAT.type->{
                        var jamFilter= jam.filter{it.typePsikolog=="Konselor"}
                        listJam= jamFilter.filter { it.isAvailable && it.getFullDate() == selectedDate }.map { it.timeStart }
                    }
                    CurhatType.ECOUNSELING.type, CurhatType.COUNSELINGCORNER.type ->{
                        var jamFilter = jam.filter{it.idPsikolog== selectedPsikolog?.id.orEmpty()}
                        listJam= jamFilter.filter { it.isAvailable && it.getFullDate() == selectedDate }.map { it.timeStart }
                    }
                }

//                var schedulePsikolog = SchedulePsikolog


                setUpSpinnerJam(listJam.toMutableList())


                if (listJam.isEmpty()){
                    spinnerJam.visibility = View.GONE
                    tvNoSlot.visibility = View.VISIBLE
                    appCompatTextView32.visibility = View.GONE
                    appCompatTextView3.visibility = View.GONE
                    tvTimeAkhir.visibility = View.GONE
                    appCompatImageView3.visibility = View.GONE
                    appCompatImageView4.visibility = View.GONE
                    mbSaveTime.visibility = View.GONE
                }
                else{
                    spinnerJam.visibility = View.VISIBLE
                    appCompatTextView32.visibility = View.VISIBLE
                    tvNoSlot.visibility = View.GONE
                    appCompatTextView32.visibility = View.VISIBLE
                    appCompatTextView3.visibility = View.VISIBLE
                    tvTimeAkhir.visibility = View.VISIBLE
                    appCompatImageView3.visibility = View.VISIBLE
                    appCompatImageView4.visibility = View.VISIBLE
                    mbSaveTime.visibility = View.VISIBLE
                    mbSaveTime.isEnabled=true

                }
            }
    }

    fun validate(): Boolean {

        var date = tvDate.text.toString()
        var hour = spinnerJam.selectedItemPosition

        return (!date.isEmpty() && hour!=0)
    }

    fun validateDate(): Boolean {

        var date = tvDate.text.toString()

        return (!date.isEmpty())
    }
}