package com.ara.ibunda


import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import com.ara.ibunda.enumtype.CalenderType
import com.ara.ibunda.enumtype.MoodType
import com.ara.ibunda.model.Jurnal
import com.ara.ibunda.utils.*
import com.github.sundeepk.compactcalendarview.domain.Event
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_calender.*
import kotlinx.android.synthetic.main.fragment_jurnal.*
import kotlinx.android.synthetic.main.fragment_login.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class JurnalFragment : Fragment() {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null

    var moodSelected = MoodType.AVERANGE.type

    var jurnal: Jurnal? = null

    val events = arrayListOf<Event>()
    var jurnalList = mutableListOf<Jurnal>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_jurnal, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        var isPsikolog= arguments?.getBoolean("isPsikolog")?:false


        if (isPsikolog){
            jurnal= arguments?.getParcelable("Jurnal")
            jurnal?.let { showJurnal(it) }
        }
        else{
            getJurnal()
        }

        mbSaveJurnal.setOnClickListener {
            doSaveJurnal()
            contentLoadingProgressBar.visibility = View.VISIBLE

        }

        dateJurnal.text = getCurrentDate()
        happy.setOnClickListener {
            onMoodSelectListener(R.id.happy)
            moodSelected = MoodType.HAPPY.type
        }
        smile.setOnClickListener {
            onMoodSelectListener(R.id.smile)
            moodSelected = MoodType.SMILE.type
        }
        averange.setOnClickListener {
            onMoodSelectListener(R.id.averange)
            moodSelected = MoodType.AVERANGE.type
        }
        sad.setOnClickListener {
            onMoodSelectListener(R.id.sad)
            moodSelected = MoodType.SAD.type
        }
        cry.setOnClickListener {
            onMoodSelectListener(R.id.cry)
            moodSelected = MoodType.CRY.type
        }


        calenderJurnal.setOnClickListener { getSelectedDateFromDatePicker() }

    }

    fun getSelectedDateFromDatePicker() {
        showCustomCalendarViewDialog(
            context = requireContext(),
            onDateClicked = {selectedDate->
                val fullDateSelected = getFullDate(selectedDate ?: Date())
                val jurnal = jurnalList.find { it.fullDate == fullDateSelected }
                jurnal?.let {
                    showJurnal(it)
                }
            },
            events = events
        )
    }

    fun onMoodSelectListener(selectedImageID: Int) {
        var moodList = mutableListOf(
            Pair(happy, 1), Pair(smile, 2), Pair(averange, 3), Pair(sad, 4), Pair(cry, 5)
        )
        moodList.forEach {
            onSetImageMood(it.first.id == selectedImageID, it.second)
        }
    }

    fun onSetImageMood(isSelected: Boolean, mood: Int) {
        when (mood) {
            1 -> happy.setImageResource(if (isSelected) R.drawable.ic_happy_color else R.drawable.ic_happy_uncolor)
            2 -> smile.setImageResource(if (isSelected) R.drawable.ic_smile_color else R.drawable.ic_smile_uncolor)
            3 -> averange.setImageResource(if (isSelected) R.drawable.ic_averange_color else R.drawable.ic_averange_uncolor)
            4 -> sad.setImageResource(if (isSelected) R.drawable.ic_sad_color else R.drawable.ic_sad_uncolor)
            5 -> cry.setImageResource(if (isSelected) R.drawable.ic_crying_color else R.drawable.ic_crying_uncolor)
        }

    }

    fun doSaveJurnal() {

        var jurnal = Jurnal(
            idUser= firebaseAuthentication?.currentUser?.uid.orEmpty(),
            dateJurnal = Date().toFirestoreTimestamp(),
            fullDate = getFullDate(Date()),
            mood = moodSelected,
            quetionOne = etQ1.text.toString(),
            quetionTwo = etQ2.text.toString(),
            quetionThree = etQ3.text.toString(),
            quetionFour = etQ4.text.toString(),
            quetionFive = etQ5.text.toString()
        )

        firebaseFireStore?.collection("Jurnal")
            ?.add(jurnal)
            ?.addOnSuccessListener {
                Toast.makeText(requireContext(), "Berhasil menyimpn jurnal pada hari ini", Toast.LENGTH_SHORT)
                    .show()
                contentLoadingProgressBar.visibility = View.GONE
                resetPage()
            }
            ?.addOnFailureListener {
                Toast.makeText(requireContext(), "Gagal menyimpan jurnal ${it.message}", Toast.LENGTH_SHORT)
                    .show()
                contentLoadingProgressBar.visibility = View.GONE
            }


    }

    fun resetPage(){
        happy.setImageResource(R.drawable.ic_happy_uncolor)
        smile.setImageResource(R.drawable.ic_smile_uncolor)
        averange.setImageResource(R.drawable.ic_averange_uncolor)
        sad.setImageResource(R.drawable.ic_sad_uncolor)
        cry.setImageResource(R.drawable.ic_crying_uncolor)
        etQ1.setText("")
        etQ2.setText("")
        etQ3.setText("")
        etQ4.setText("")
        etQ5.setText("")
    }

    fun getJurnal(){
        contentLoadingProgressBar.visibility = View.VISIBLE
        val idUser = firebaseAuthentication?.currentUser?.uid.orEmpty()

        firebaseFireStore?.collection("Jurnal")
            ?.whereEqualTo("idUser", idUser)
            ?.get()
            ?.addOnSuccessListener{
                contentLoadingProgressBar.visibility = View.GONE
                jurnalList = it.toObjects(Jurnal::class.java)

                val currentDate = jurnalList.map { it.dateJurnal.toDate() }

                currentDate.forEach {
                    val calendar = Calendar.getInstance()
                    calendar.time = it
                    events.add(Event(R.color.colorAccent, it.time, "ada data"))
                }

            }
            ?.addOnFailureListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
            }
    }

    private fun showJurnal(jurnal:Jurnal){
        etQ1.setText(jurnal.quetionOne)
        etQ2.setText(jurnal.quetionTwo)
        etQ3.setText(jurnal.quetionThree)
        etQ4.setText(jurnal.quetionFour)
        etQ5.setText(jurnal.quetionFive)
        onSetImageMood(true, jurnal.mood)
        calenderJurnal.visibility = View.GONE
        mbSaveJurnal.visibility = View.GONE
        etQ1.isEnabled=false
        etQ2.isEnabled=false
        etQ3.isEnabled=false
        etQ4.isEnabled=false
        etQ5.isEnabled=false
    }

    fun validate(): Boolean {

        var email = etEmailSignin.text.toString()
        var password = etPasswordSignin.text.toString()

        return (!email.isEmpty() && !password.isEmpty())
    }

}
