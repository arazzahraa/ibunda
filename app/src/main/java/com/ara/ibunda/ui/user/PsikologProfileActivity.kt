package com.ara.ibunda

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ara.ibunda.model.Psikolog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_psikolog_profile.*
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.activity_summary.tvNamaPsikolog
import kotlinx.android.synthetic.main.item_psikolog.view.*

class PsikologProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_psikolog_profile)

        var psikolog = intent.getParcelableExtra<Psikolog>("psikolog")
        tvNamaPsikolog.setText(psikolog.name)
        tvAgePsikolog.setText(psikolog.age)
        tvJenisKelaminPsikolog.setText(psikolog.gender)
        tvTypePsi.setText(psikolog.typePsikolog)
        var requestOption = RequestOptions()
            .placeholder(R.drawable.ic_profile)
            .error(R.drawable.ic_profile)
        Glide.with(this).load(psikolog.avatar).listener(object :
            RequestListener<Drawable> {
            override fun onResourceReady(
                resource: Drawable?,
                model: Any?,
                target: com.bumptech.glide.request.target.Target<Drawable>?,
                dataSource: com.bumptech.glide.load.DataSource?,
                isFirstResource: Boolean
            ): Boolean {
                return false
            }

            override fun onLoadFailed(
                e: GlideException?,
                model: Any?,
                target: com.bumptech.glide.request.target.Target<Drawable>?,
                isFirstResource: Boolean
            ): Boolean {
                circleImageView.setImageResource(R.drawable.ic_profile)
                return false
            }

        }).apply(requestOption).into(circleImageView)
    }




}
