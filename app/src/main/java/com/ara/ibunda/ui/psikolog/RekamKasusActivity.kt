package com.ara.ibunda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.enumtype.RekamKasusType
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.Patient
import com.ara.ibunda.model.RekamKasus
import com.ara.ibunda.utils.getCurrentDate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_rekam_kasus.*
import kotlinx.android.synthetic.main.item_chat_counseling.*

class RekamKasusActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var konseling: Konseling? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rekam_kasus)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        val typeRekamKasus = intent.getIntExtra("rekam kasus", RekamKasusType.ISI.type)
        val rekamKasus = intent.getParcelableExtra<RekamKasus>("rekam kasus data")

        konseling= intent.getParcelableExtra("Konseling")
        tvNamaPatient.setText(konseling?.patientName.orEmpty())

        when (typeRekamKasus) {
            RekamKasusType.ISI.type -> {
                tvDate.setText(getCurrentDate())
            }
            RekamKasusType.LIHAT.type->{
                tvDate.setText(rekamKasus?.date)
                etSubyektif.setText(rekamKasus?.subyektif)
                etObyektif.setText(rekamKasus?.obyektif)
                etAssesment.setText(rekamKasus?.assesment)
                etPlanning.setText(rekamKasus?.planning)
                mbSaveRekamKasus.visibility = View.GONE
            }
        }

        mbSaveRekamKasus.setOnClickListener {
            contentLoadingProgressBar.visibility = View.VISIBLE
            if (validate()) {
                doSaveDataRekamKasus()
            } else {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(
                    this,
                    "Silahkan isi email dan password anda",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }

    }

    fun doSaveDataRekamKasus(){

        var rekamKasus= RekamKasus(
            idUser = konseling?.idUser.orEmpty(),
            idPsikolog = firebaseAuthentication?.currentUser?.uid.orEmpty(),
            date = tvDate.text.toString(),
            subyektif= etSubyektif.text.toString(),
            obyektif = etObyektif.text.toString(),
            assesment = etAssesment.text.toString(),
            planning = etPlanning.text.toString()
        )

        firebaseFireStore?.collection("RekamKasus")
            ?.add(rekamKasus)
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                var idRekamKasus= it.id
                firebaseFireStore?.collection("RekamKasus")
                    ?.document(idRekamKasus)
                    ?.update("id", idRekamKasus)
                    ?.addOnSuccessListener {
                        contentLoadingProgressBar.visibility = View.GONE
                        doUpdateKonseling()
                    }
                    ?.addOnFailureListener {
                        contentLoadingProgressBar.visibility = View.GONE
                        Toast.makeText(this, "Gagal menyimpan rekam kasus ${it.message}", Toast.LENGTH_SHORT).show()}
            }
            ?.addOnFailureListener {
                contentLoadingProgressBar.visibility = View.GONE
                Toast.makeText(this, "Gagal menyimpan rekam kasus ${it.message}", Toast.LENGTH_SHORT).show()

            }
    }

    fun doUpdateKonseling(){
        konseling?.let{
            firebaseFireStore?.collection("Konseling")
                ?.document(it.id)
                ?.update("paymentStatus", PaymentStatusType.SELESAI.type)
                ?.addOnSuccessListener {
                    resetPage()
                    contentLoadingProgressBar.visibility = View.GONE
                    Toast.makeText(
                        this,
                        "Berhasil Menyimpan Rekam Kasus",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                ?.addOnFailureListener {
                    contentLoadingProgressBar.visibility = View.GONE
                    Toast.makeText(this, "Gagal menyimpan rekam kasus ${it.message}", Toast.LENGTH_SHORT).show()}
        }
    }

    fun validate(): Boolean {

        var subyektif = etSubyektif.text.toString()
        var obyektif = etObyektif.text.toString()
        var asesment  = etAssesment.text.toString()
        var planning = etPlanning.text.toString()

        return (!subyektif.isEmpty() && !obyektif.isEmpty() && !asesment.isEmpty() && !planning.isEmpty())
    }

    fun resetPage(){
        etSubyektif.setText("")
        etObyektif.setText("")
        etAssesment.setText("")
        etPlanning.setText("")
    }
}
