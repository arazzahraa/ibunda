package com.ara.ibunda

import android.content.Intent
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.*
import com.ara.ibunda.utils.getCurrentTime
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.item_chat_counseling.view.*

class SummaryActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuthentication: FirebaseAuth? = null
    var type: Int = 0
    var psikologSelected: Psikolog? = null
    var selectedCity: String = ""
    var selectedAddress: String = ""

    var idPsikolog: String = ""

    var idSchedule : String = ""

    var preference:IbundaPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)

        contentLoadingProgressBar.visibility = View.VISIBLE

        type = intent.getIntExtra("curhat", CurhatType.ECOUNSELING.type)
        val dateSeleceted = intent.getStringExtra("tgl terpilih").orEmpty()
        val hourSeleceted = intent.getStringExtra("jam terpilih").orEmpty()
        psikologSelected = intent.getParcelableExtra("selected psikolog")
        selectedCity = intent.getStringExtra("selected city").orEmpty()
        selectedAddress = intent.getStringExtra("selected address").orEmpty()

        val isChat = intent.getBooleanExtra("summary", false)
        var konseling: Konseling? = intent.getParcelableExtra("Konseling")

        preference = IbundaPreference(this)
        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseAuthentication = FirebaseAuth.getInstance()

        mbDoneSummary.text = if (isChat) "Chat" else "Done"
        if (isChat) {
            when (type) {
                CurhatType.CURHAT.type, CurhatType.ECOUNSELING.type -> {
                    dateKon.text = konseling?.date
                    hourKon.text = konseling?.hour
                    getPsikolog(konseling?.idPsikolog.orEmpty())
                    tvSummartyTitle.text = "Kamu akan bertemu bunda pada:"
                    grpBayar.visibility = View.GONE

                }
                CurhatType.COUNSELINGCORNER.type -> {
                    dateKon.text = konseling?.date
                    hourKon.text = konseling?.hour
                    getPsikolog(konseling?.idPsikolog.orEmpty())
                    getLocation(konseling?.location.orEmpty())
                    tvSummartyTitle.text = "Kamu akan bertemu bunda pada:"
                    grpBayar.visibility = View.GONE
                    mbDoneSummary.visibility = View.GONE
                }
            }
        } else {
            dateKon.text = dateSeleceted
            hourKon.text = hourSeleceted
            getSchedulePsikolog(dateSeleceted, hourSeleceted)
            getLocation(selectedAddress)
        }

        when (type) {
            CurhatType.CURHAT.type -> {
                grpTempat.visibility = View.GONE
                grpBayar.visibility = View.GONE
                tvService.text = "Curhat"
            }
            CurhatType.ECOUNSELING.type -> {
                grpTempat.visibility = View.GONE
                tvService.text = "E-Counseling"
            }
            CurhatType.COUNSELINGCORNER.type -> {
                tvService.text = "Counseling Corner"
            }
        }

        tvCaraBayar.setOnClickListener {
            val intent = Intent(this, HowToPayActivity::class.java)
            startActivity(intent)
        }

        mbDoneSummary.setOnClickListener {
            preference?.saveInt("curhatType", type)
            if (isChat) {
//                if (getCurrentTime()!=konseling?.hour){
//                    Toast.makeText(
//                        this,
//                        "Silahkan akses kembali setelah jam konseling sesuai",
//                        Toast.LENGTH_SHORT
//                    ).show()
//                }
//                else{
                    val intent = Intent(this, ChatActivity::class.java)
                    intent.putExtra("Konseling", konseling)
                    startActivity(intent)
//                }
            } else {
                contentLoadingProgressBar.visibility = View.VISIBLE
                doRegisterKonseling(
                    konseling = Konseling(
                        idSchedule = idSchedule,
                        service = type,
                        idUser = firebaseAuthentication?.currentUser?.uid.orEmpty(),
                        patientName = preference?.getString("USER_NAME").orEmpty(),
                        patientAvatar = preference?.getString("USER_AVATAR").orEmpty(),
                        date = dateKon.text.toString(),
                        hour = hourKon.text.toString(),
                        location = tvAlamat.text.toString(),
                        idPsikolog = idPsikolog,
                        psikolog = tvNamaPsikolog.text.toString(),
                        psikologAvatar = psikologSelected?.avatar.orEmpty(),
                        price = tvTotalBayar.text.toString(),
                        paymentStatus = if (type == CurhatType.CURHAT.type) PaymentStatusType.BAYAR.type else PaymentStatusType.BELUMBAYAR.type,
                        typePsikolog = psikologSelected?.typePsikolog ?: "Konselor"
                    )
                )
            }
        }
    }

    fun getSchedulePsikolog(selectedDate: String, timeStart: String) {
        firebaseFireStore?.collection("SchedulePsikolog")
            ?.whereEqualTo("timeStart", timeStart)
            ?.whereEqualTo("fullDate", selectedDate)
            ?.get()
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                var listSchedule = it.toObjects(SchedulePsikolog::class.java)
                when (type) {
                    CurhatType.CURHAT.type -> {
                        var schedule =
                            listSchedule.filter { it.isAvailable && it.getFullDate() == selectedDate }
                                .firstOrNull()
                        idPsikolog = schedule?.idPsikolog.orEmpty()
                        idSchedule = schedule?.id.orEmpty()
                        getPsikolog(idPsikolog.orEmpty())
                    }
                    CurhatType.ECOUNSELING.type, CurhatType.COUNSELINGCORNER.type -> {
                        var schedule = listSchedule.filter { it.idPsikolog == psikologSelected?.id }
                            .firstOrNull()
                        idPsikolog = schedule?.idPsikolog.orEmpty()
                        idSchedule = schedule?.id.orEmpty()
                        getPsikolog(idPsikolog.orEmpty())
                    }
                }

            }
    }

    fun getLocation(address: String) {
        firebaseFireStore?.collection("Location")
            ?.whereEqualTo("address", address)
            ?.get()
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                val location = it.toObjects(Location::class.java).firstOrNull()
                tvNamaTempat.text = location?.name
                tvAlamat.text = location?.address
            }
    }

    fun getPsikolog(id: String) {
        firebaseFireStore?.collection("User")?.document(id)
            ?.get()
            ?.addOnSuccessListener {
                contentLoadingProgressBar.visibility = View.GONE
                var psikolog = it.toObject(User::class.java)
                tvNamaPsikolog.text = psikolog?.namaLengkap
                var requestOption = RequestOptions()
                    .placeholder(R.drawable.ic_profile)
                    .error(R.drawable.ic_profile)
                Glide.with(this).load(psikolog?.avatar).listener(object:
                    RequestListener<Drawable> {
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        dataSource: com.bumptech.glide.load.DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }

                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        circleImageView3.setImageResource(R.drawable.ic_profile)
                        return false
                    }

                }).apply(requestOption).into(circleImageView3)
                when (psikolog?.typePsikolog) {
                    "Basic" -> tvTotalBayar.text = "Rp 169.000"
                    "Essential" -> tvTotalBayar.text = "Rp 279.000"
                    "Profesional" -> tvTotalBayar.text = "Rp 389.000"
                    "Premium" -> tvTotalBayar.text = "Rp 558.000"
                }
            }
    }

    fun doRegisterKonseling(konseling: Konseling) {
        firebaseFireStore?.collection("Konseling")
            ?.add(konseling)
            ?.addOnSuccessListener {
                var idKonseling = it.id
                firebaseFireStore?.collection("Konseling")?.document(idKonseling)
                    ?.update("id", idKonseling)
                    ?.addOnSuccessListener {
                        contentLoadingProgressBar.visibility = View.GONE
                        when (type) {
                            CurhatType.CURHAT.type -> {
                                Toast.makeText(
                                    this,
                                    "Pendaftaran berhasil",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val intent = Intent(this, MainActivity::class.java)
                                intent.putExtra("curhat", CurhatType.CURHAT.type)
                                intent.putExtra("schedule", true)
                                startActivity(intent)
                            }
                            CurhatType.ECOUNSELING.type, CurhatType.COUNSELINGCORNER.type -> {
                                Toast.makeText(
                                    this,
                                    "Pendaftaran berhasil, silahkan unggah bukti bayar",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val intent = Intent(this, UploadPaymentReceiptActivity::class.java)
                                intent.putExtra("idKonseling", idKonseling)
                                startActivity(intent)
                            }

                        }
                    }

            }
            ?.addOnFailureListener {
                Toast.makeText(this, "Pendaftaran gagal ${it.message}", Toast.LENGTH_SHORT).show()
                contentLoadingProgressBar.visibility = View.GONE
            }


    }
}





