package com.ara.ibunda

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isEmpty
import com.ara.ibunda.enumtype.UserType
import com.ara.ibunda.model.Psikolog
import com.ara.ibunda.model.User
import com.ara.ibunda.utils.getSelectedDate
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_plus_psikolog.*
import kotlinx.android.synthetic.main.fragment_signup.*
import java.util.*

class PlusPsikologActivity : AppCompatActivity() {

    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_psikolog)

        setUpSpinnerGender()
        setUpSpinnerPsikolog()
        setUpSpinnerDomisiliPsikolog()

        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        mbSavePsikolog.setOnClickListener {

            if (validate()) {
                var email = tiIsiEmailPsikolog.text.toString()
                var password = tiIsiPasswordPsikolog.text.toString()
                doSignUpPsikolog(email, password)
            } else {
                Toast.makeText(
                    this,
                    "Silahkan lengkapi data akun psikolog",
                    Toast.LENGTH_SHORT
                ).show()
            }


        }

        tiIsiUmurPsikolog.setOnClickListener { getSelectedDateFromDatePicker() }

    }

    fun getSelectedDateFromDatePicker() {
        var calender = Calendar.getInstance()
        var selectedYear = calender.get(Calendar.YEAR)
        var selectedMonth = calender.get(Calendar.MONTH)
        var selectedDay = calender.get(Calendar.DATE)

        var datePickerDialog: DatePickerDialog = DatePickerDialog(
            this, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                calender.set(Calendar.YEAR, year)
                calender.set(Calendar.MONTH, month)
                calender.set(Calendar.DATE, dayOfMonth)
                val selectedDate = calender.time
                val selectedDateInStrig = getSelectedDate(selectedDate)

                tiIsiUmurPsikolog.setText(selectedDateInStrig)
            }, selectedYear, selectedMonth, selectedDay
        )
        datePickerDialog.show()
    }

    fun setUpSpinnerGender() {
        var gender: MutableList<String> = mutableListOf("Jenis Kelamin", "Perempuan", "Laki-laki")
        var genderAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)
        genderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerGenderPlusPsikolog.adapter = genderAdapter
    }

    fun setUpSpinnerPsikolog() {
        var gender: MutableList<String> = mutableListOf(
            "Psikolog",
            "Basic",
            "Essential",
            "Profesional",
            "Premium",
            "Konselor"
        )
        var genderAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, gender)
        genderAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerJenisPsikolog.adapter = genderAdapter
    }

    fun setUpSpinnerDomisiliPsikolog() {
        var domisili: MutableList<String> = mutableListOf(
            "Domisili",
            "Jakarta",
            "Bandung",
            "Surabaya",
            "Bali"
        )
        var domisiliAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, domisili)
        domisiliAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerDomisiliPsikolog.adapter = domisiliAdapter
    }

    fun doSaveDataPsikolog(idPsikolog: String) {

        var psikolog = User(
            id = idPsikolog,
            namaLengkap = tiIsiNamaPsikolog.text.toString(),
            email = tiIsiEmailPsikolog.text.toString(),
            gender = spinnerGenderPlusPsikolog.selectedItem.toString(),
            age = tiIsiUmurPsikolog.text.toString(),
            typePsikolog = spinnerJenisPsikolog.selectedItem.toString(),
            domisili = spinnerDomisiliPsikolog.selectedItem.toString(),
            pengalaman = tiIsiPengalamanPsikolog.text.toString(),
            password = tiIsiPasswordPsikolog.text.toString(),
            status = UserType.PSIKOLOG.type

        )

        firebaseFireStore?.collection("User")?.document(idPsikolog)
            ?.set(psikolog)
            ?.addOnSuccessListener {
                resetPage()
                Toast.makeText(this, "Berhasil Menambahkan Akun Psikolog", Toast.LENGTH_SHORT)
                    .show()
            }
            ?.addOnFailureListener {
                Toast.makeText(this, "Gagal menyimpan ${it.message}", Toast.LENGTH_SHORT).show()

            }
    }

    fun doSignUpPsikolog(email: String, password: String) {
        firebaseAuthentication?.createUserWithEmailAndPassword(email, password)
            ?.addOnCompleteListener {
                if (it.isSuccessful) {
                    var idUser = it.result?.user?.uid
                    if (idUser != null) {
                        doSaveDataPsikolog(idPsikolog = idUser)
                    }
                    Toast.makeText(this, "Berhasil Menambahkan Akun Psikolog", Toast.LENGTH_SHORT)
                        .show()

                } else Toast.makeText(
                    this,
                    "Gagal Menambahkan Akun Psikolog ${it.exception?.message}",
                    Toast.LENGTH_SHORT
                ).show()

            }
    }

    fun validate(): Boolean {

        var nama = tiIsiNamaPsikolog.text.toString()
        var email = tiIsiEmailPsikolog.text.toString()
        var jenisKelamin = spinnerGenderPlusPsikolog.isSelected.toString()
        var umur = tiIsiUmurPsikolog.text.toString()
        var domisili = spinnerDomisiliPsikolog.isSelected.toString()
        var pengalaman = tiIsiPengalamanPsikolog.text.toString()
        var typePsikolog = spinnerJenisPsikolog.isSelected.toString()
        var password = tiIsiPasswordPsikolog.text.toString()

        return (!nama.isEmpty() &&
                !email.isEmpty() &&
                !jenisKelamin.isEmpty() &&
                !umur.isEmpty() &&
                !domisili.isEmpty() &&
                !pengalaman.isEmpty() &&
                !typePsikolog.isEmpty() &&
                !password.isEmpty())
    }

    fun resetPage() {
        tiIsiNamaPsikolog.setText("")
        tiIsiEmailPsikolog.setText("")
        tiIsiUmurPsikolog.setText("")
        tiIsiPasswordPsikolog.setText("")
        tiIsiPengalamanPsikolog.setText("")

        spinnerGenderPlusPsikolog.setSelection(0)
        spinnerJenisPsikolog.setSelection(0)
        spinnerDomisiliPsikolog.setSelection(0)


    }
}
