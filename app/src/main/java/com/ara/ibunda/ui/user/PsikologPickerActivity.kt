package com.ara.ibunda

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.adapter.LocationAdapter
import com.ara.ibunda.adapter.PsikologAdapter
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.model.Psikolog
import com.ara.ibunda.model.User
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_chat_infomation.*
import kotlinx.android.synthetic.main.activity_psikolog_picker.*
import kotlinx.android.synthetic.main.item_location.*
import kotlinx.android.synthetic.main.item_psikolog.*
import kotlinx.android.synthetic.main.item_psikolog.view.*

class PsikologPickerActivity : AppCompatActivity() {

    var psikolog: MutableList <Psikolog> = mutableListOf()
    var firebaseFireStore : FirebaseFirestore? = null
    var selectedCity : String=""
    var selectedAddress : String=""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_psikolog_picker)

        val typePsikolog= intent.getStringExtra("typePsikolog")
        selectedCity = intent.getStringExtra("selected city").orEmpty()
        selectedAddress = intent.getStringExtra("selected address").orEmpty()


        firebaseFireStore = FirebaseFirestore.getInstance()

        getPsikolog(typePsikolog)
    }

    fun getPsikolog(type : String){
        firebaseFireStore?.collection("User")
            ?.whereEqualTo("status", "Psikolog")
            ?.whereEqualTo("typePsikolog", type)
            ?.get()
            ?.addOnSuccessListener {
                var psikolog = it.toObjects(User::class.java)

                if(selectedCity.isNotEmpty()){
                    var psikologFilter = psikolog.filter {it.domisili==selectedCity}
                    setUpPsikologList(psikologFilter.toMutableList())
                }
                else{
                    setUpPsikologList(psikolog)
                }

            }
    }

    fun setUpPsikologList(psikolog: MutableList<User>) {
        val type=intent.getIntExtra("curhat",CurhatType.ECOUNSELING.type)
        var psikologAdapter = PsikologAdapter(this, psikolog.map { it.toPsikolog() }.toMutableList()) {
            when(type){
                CurhatType.ECOUNSELING.type->{
                    val intent= Intent(this, CalenderActivity::class.java)
                    intent.putExtra("curhat", CurhatType.ECOUNSELING.type)
                    intent.putExtra("selected psikolog", it)
                    startActivity(intent)
                }
                CurhatType.COUNSELINGCORNER.type->{
                    val intent= Intent(this, CalenderActivity::class.java)
                    intent.putExtra("curhat", CurhatType.COUNSELINGCORNER.type)
                    intent.putExtra("selected psikolog", it)
                    intent.putExtra("selected city", selectedCity)
                    intent.putExtra("selected address", selectedAddress)
                    startActivity(intent)
                }
            }
        }

        rvPsikolog.apply {
            layoutManager= LinearLayoutManager(this@PsikologPickerActivity)
            adapter=psikologAdapter
            addItemDecoration(
                DividerItemDecoration(this@PsikologPickerActivity,
                    DividerItemDecoration.VERTICAL)
            )
        }
    }
}
