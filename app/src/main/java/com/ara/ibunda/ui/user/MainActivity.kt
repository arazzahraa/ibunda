package com.ara.ibunda

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import com.ara.ibunda.utils.replaceFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.ara.ibunda.SharedPreference.IbundaPreference
import com.ara.ibunda.adapter.PsikologAdapter
import com.ara.ibunda.enumtype.CurhatType
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.Konseling
import com.ara.ibunda.model.User
import com.ara.ibunda.ui.user.HistoryKonselingActivity
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_chat.*


class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    var isAnySchedule = false
    var currentMenuItem : MenuItem?=null
    var isMenuChat = false
    var settingMenu : Menu?=null
    var firebaseAuthentication: FirebaseAuth? = null
    var firebaseFireStore: FirebaseFirestore? = null
    var preference: IbundaPreference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainMenu.setOnNavigationItemSelectedListener(this)
        replaceFragment(R.id.frameLayout, ChatFragment(), false)

        firebaseAuthentication = FirebaseAuth.getInstance()
        firebaseFireStore = FirebaseFirestore.getInstance()

        currentMenuItem= mainMenu.menu.findItem(mainMenu.selectedItemId)
        isMenuChat= currentMenuItem?.itemId==R.id.Chat
        isAnySchedule=intent.getBooleanExtra("schedule", false )

        preference= IbundaPreference(this)

        setSupportActionBar(toolbar)

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.Chat ->{
                replaceFragment(R.id.frameLayout, ChatFragment(), false)
                return true
            }

            R.id.Jurnal ->{
                replaceFragment(R.id.frameLayout, JurnalFragment(), false)
                return true
            }

            R.id.Profile ->{
                replaceFragment(R.id.frameLayout, ProfileFragment(), false)
                return true
            }
        }
    return false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        settingMenu=menu
        menuInflater.inflate(R.menu.menu_login,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.faq->{
                val intent=Intent(this, FaqActivity::class.java)
                startActivity(intent)
            }
            R.id.sDank->{
                val intent=Intent(this, SAndKActivity::class.java)
                startActivity(intent)
            }
            R.id.tentangKami->{
                val intent=Intent(this, AboutUsActivity::class.java)
                startActivity(intent)
            }
            R.id.riwayatKonseling->{
                val intent=Intent(this, HistoryKonselingActivity::class.java)
                startActivity(intent)
            }
            R.id.keluar->{
                firebaseAuthentication?.signOut()
                preference?.saveBoolean("IS_LOGGED_IN", false)
                preference?.saveString("USER_TYPE", "")
                val intent= Intent (this, SignupActivity::class.java)
                startActivity(intent)
                finishAffinity()
            }
        }
            return true
    }

    fun getKonseling(){
        firebaseFireStore?.collection("Konseling")
            ?.whereEqualTo("idUser", firebaseAuthentication?.currentUser?.uid)
            ?.get()
            ?.addOnSuccessListener {
                var konseling= it.toObjects(Konseling::class.java).filter { it.paymentStatus!= PaymentStatusType.SELESAI.type }.firstOrNull()
                tvSchedule.visibility= if(konseling!=null) View.VISIBLE else View.GONE

                when(konseling?.paymentStatus){
                    PaymentStatusType.BELUMBAYAR.type->{
                        tvSchedulee.setText("Kamu belum melakukan pembayaran")
                    }
                    PaymentStatusType.MENUNGGUKONFIRMASI.type->{
                        tvSchedulee.setText("Tunggu konfirmasi pembayaran dari bunda ya!")
                    }
                    PaymentStatusType.BAYAR.type->{
                        tvSchedulee.setText("Kamu punya jadwal bertemu dengan bunda yaa")
                    }
                    PaymentStatusType.SELESAI.type ->{
                        tvSchedule.visibility = View.GONE
                    }
                    PaymentStatusType.REJECT.type ->{
                        tvSchedulee.setText("Mohon maaf pembayaran anda telah ditolak,silahkan dan upload ulang bukti pembayaran dengan benar")
                    }
                }

                tvSchedule.setOnClickListener{

                        when (konseling?.service){
                            CurhatType.CURHAT.type->{
                                val intent = Intent (this, SummaryActivity::class.java)
                                intent.putExtra("summary",true)
                                intent.putExtra("curhat", konseling?.service)
                                intent.putExtra("Konseling", konseling)
                                startActivity(intent)
                            }
                            CurhatType.ECOUNSELING.type, CurhatType.COUNSELINGCORNER.type->{
                                when(konseling.paymentStatus){
                                    PaymentStatusType.BELUMBAYAR.type->{
                                        val intent = Intent (this, UploadPaymentReceiptActivity::class.java)
                                        intent.putExtra("idKonseling", konseling.id)
                                        startActivity(intent)
                                    }
                                    PaymentStatusType.BAYAR.type->{
                                        val intent = Intent (this, SummaryActivity::class.java)
                                        intent.putExtra("summary",true)
                                        intent.putExtra("curhat", konseling?.service)
                                        intent.putExtra("Konseling", konseling)
                                        startActivity(intent)
                                    }
                                    PaymentStatusType.REJECT.type->{
                                        val intent = Intent (this, UploadPaymentReceiptActivity::class.java)
                                        intent.putExtra("idKonseling", konseling.id)
                                        startActivity(intent)
                                    }
                                }
                            }
                            else->{
                                val intent = Intent (this, SummaryActivity::class.java)
                                intent.putExtra("summary",true)
                                intent.putExtra("curhat", konseling?.service)
                                intent.putExtra("Konseling", konseling)
                                startActivity(intent)
                            }

                        }

                }
            }
    }

    override fun onResume() {
        getKonseling()
        super.onResume()
    }



}
