package com.ara.ibunda

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.ara.ibunda.enumtype.PaymentStatusType
import com.ara.ibunda.model.Payment
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.features.ReturnMode
import com.esafirm.imagepicker.model.Image
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_summary.*
import kotlinx.android.synthetic.main.activity_upload_payment_receipt.*
import java.io.File
import java.net.URL

class UploadPaymentReceiptActivity : AppCompatActivity() {

    var firebaseFireStore: FirebaseFirestore? = null
    var firebaseAuth: FirebaseAuth? = null
    var firebaseStorage: FirebaseStorage? = null
    var storageReference: StorageReference? = null
    var idKonseling: String = ""
    var path: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_payment_receipt)

        firebaseFireStore = FirebaseFirestore.getInstance()
        firebaseStorage = FirebaseStorage.getInstance()
        firebaseAuth = FirebaseAuth.getInstance()
        storageReference = firebaseStorage?.getReference()?.child("upload receipt payment")

        idKonseling = intent.getStringExtra("idKonseling").orEmpty()


        ivImgFromCamera.setOnClickListener {
            setUpImageFromCamera()
        }

        ivImgFromGallery.setOnClickListener {
            setUpImageFromGallery()
        }

        btnNextPayment.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("schedule", true)
            startActivity(intent)
        }

        btnNextPayment.setOnClickListener {
            if (validate()) {
                contentLoadingProgressBarPay.visibility = View.VISIBLE
                doSavePhotoReciept(path)
            } else {
                Toast.makeText(
                    this,
                    "Silahkan unggah bukti bayar kamu",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarPay.visibility = View.GONE
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            var image: Image = ImagePicker.getFirstImageOrNull(data)
            path = image.path
            Glide.with(this).load(path).into(ivReceipt)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    fun setUpImageFromCamera() {
        ImagePicker.cameraOnly().start(this)
    }

    fun setUpImageFromGallery() {
        ImagePicker.create(this)
            .returnMode(ReturnMode.GALLERY_ONLY) // set whether pick and / or camera action should return immediate result or not.
            .folderMode(true) // folder mode (false by default)
            .toolbarFolderTitle("Folder") // folder selection title
            .toolbarImageTitle("Tap to select") // image selection title
            .toolbarArrowColor(Color.BLACK) // Toolbar 'up' arrow color
            .single() // single mode
            .showCamera(true) // show camera or not (true by default)
            .imageDirectory("Camera") // directory name for captured image  ("Camera" folder by default)
            .enableLog(false) // disabling log
            .start(); // start image picker activity with request code
    }

    fun pendingPayment() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onBackPressed() {
        pendingPayment()
        super.onBackPressed()
    }

    fun doSavePhotoReciept(imagePath: String){
        var file= Uri.fromFile(File(imagePath))
        var document = storageReference?.child("${file.lastPathSegment}")

        var uploadTask= document?.putFile(file)

        uploadTask?.continueWithTask {task->
            if(!task.isSuccessful){
                task.exception?.let {
                    throw it
                }
            }
            document?.downloadUrl
        }?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val downloadUrl = task.result.toString()
                doSavePayment(downloadUrl)
            } else {
                Toast.makeText(
                    this,
                    "Gagal menyimpan bukti pembayaran ${task.exception?.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun doSavePayment(imgURL: String) {
        var payment = Payment(
            id = "",
            idUser = firebaseAuth?.currentUser?.uid.orEmpty(),
            idKonseling = idKonseling,
            paymentReceipt = imgURL,
            paymentStatus = PaymentStatusType.MENUNGGUKONFIRMASI.type
        )

        firebaseFireStore?.collection("Pembayaran")
            ?.add(payment)
            ?.addOnSuccessListener {
                contentLoadingProgressBarPay.visibility = View.GONE
                var idPayment = it.id
                firebaseFireStore?.collection("Pembayaran")
                    ?.document(idPayment)
                    ?.update("id", idPayment)
                    ?.addOnSuccessListener {
                        doUpdateKonseling()
                    }
                    ?.addOnFailureListener {
                        Toast.makeText(
                            this,
                            "Gagal menyimpan bukti pembayaran ${it.message}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            }
            ?.addOnFailureListener {
                Toast.makeText(
                    this,
                    "Gagal menyimpan bukti pembayaran ${it.message}",
                    Toast.LENGTH_SHORT
                ).show()
                contentLoadingProgressBarPay.visibility = View.GONE
            }
    }

    fun doUpdateKonseling() {
        firebaseFireStore?.collection("Konseling")?.document(idKonseling)
            ?.update("paymentStatus", PaymentStatusType.MENUNGGUKONFIRMASI.type)
            ?.addOnSuccessListener {
                Toast.makeText(
                    this,
                    "Berhasil mengunggah bukti pembayaran",
                    Toast.LENGTH_SHORT
                ).show()
                val intent = Intent(this, MainActivity::class.java)
                intent.putExtra("schedule", true)
                startActivity(intent)
            }
            ?.addOnFailureListener {
                Toast.makeText(
                    this,
                    "Gagal menyimpan bukti pembayaran ${it.message}",
                    Toast.LENGTH_SHORT
                ).show()
            }

    }


    fun validate(): Boolean {

        var buktiBayar = ivReceipt.drawable

        return buktiBayar != null
    }
}
