package com.ara.ibunda.SharedPreference

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

class IbundaPreference(context: Context) {
    var PREFERENCE_NAME = "ibunda"
    var PREFERENCE_MODE = MODE_PRIVATE
    var sharedPreference:SharedPreferences = context.getSharedPreferences(PREFERENCE_NAME, PREFERENCE_MODE)
    var editor: SharedPreferences.Editor= sharedPreference.edit()

    fun saveBoolean (name: String, value: Boolean){
        editor.putBoolean(name, value)
        editor.commit()
    }

    fun getBoolean (name: String): Boolean {
        return sharedPreference.getBoolean(name, false)
    }

    fun saveInt(name:String, value: Int){
        editor.putInt(name, value)
        editor.commit()
    }

    fun getInt(name:String): Int{
        return sharedPreference.getInt(name, 0)
    }

    fun saveString(name:String, value: String){
        editor.putString(name, value)
        editor.commit()
    }

    fun getString(name:String): String{
        return sharedPreference.getString(name, "").orEmpty()
    }

}